# 02-01: kubectl

En esta sección interactuaremos con los objetos en el clúster mediante la interfaz de línea de comandos `kubectl`.

![Estructura de Comandos](../media/02_01_LAB_01_Estructura_cmds.png)

A continuación se muestran algunos comandos de `kubectl`.

## `cluster-info`

Para desplegar la dirección del _control plane_ del clúster podemos utilizar el comando `cluster-info`.

```Shell
kubectl cluster-info
```

Salida

```Shell
Kubernetes control plane is running at https://192.168.49.2:8443
CoreDNS is running at https://192.168.49.2:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

## Versiones

### `version`

Para imprimir la información del cliente/servidor podemos recurrir al comando `version`

```Shell
kubectl version --output=yaml
```

Salida

```Shell
clientVersion:
  buildDate: "2022-02-16T12:38:05Z"
  compiler: gc
  gitCommit: e6c093d87ea4cbb530a7b2ae91e54c0842d8308a
  gitTreeState: clean
  gitVersion: v1.23.4
  goVersion: go1.17.7
  major: "1"
  minor: "23"
  platform: linux/amd64
serverVersion:
  buildDate: "2022-01-25T21:19:12Z"
  compiler: gc
  gitCommit: 816c97ab8cff8a1c72eccca1026f7820e93e0d25
  gitTreeState: clean
  gitVersion: v1.23.3
  goVersion: go1.17.6
  major: "1"
  minor: "23"
  platform: linux/amd64
```

### `api-versions`

Para imprimir las versiones de API admitidas en el servidor, en forma de _grupo/version_ puede utilizar el comando `api-versions` del CLI `kubectl`.

```Shell
kubectl api-versions
```

Salida

```Shell
admissionregistration.k8s.io/v1
apiextensions.k8s.io/v1
apiregistration.k8s.io/v1
apps/v1
authentication.k8s.io/v1
authorization.k8s.io/v1
autoscaling/v1
autoscaling/v2
autoscaling/v2beta1
autoscaling/v2beta2
batch/v1
batch/v1beta1
certificates.k8s.io/v1
coordination.k8s.io/v1
discovery.k8s.io/v1
discovery.k8s.io/v1beta1
events.k8s.io/v1
events.k8s.io/v1beta1
flowcontrol.apiserver.k8s.io/v1beta1
flowcontrol.apiserver.k8s.io/v1beta2
networking.k8s.io/v1
node.k8s.io/v1
node.k8s.io/v1beta1
policy/v1
policy/v1beta1
rbac.authorization.k8s.io/v1
scheduling.k8s.io/v1
storage.k8s.io/v1
storage.k8s.io/v1beta1
v1
```

## `api-resources`

Para conocer los recursos del API soportados podemos recurrir al comando `api-resources`.

```Shell
kubectl api-resources --namespaced=true      # All namespaced resources
kubectl api-resources --namespaced=false     # All non-namespaced resources
kubectl api-resources --output=name          # All resources with simple output (only the resource name)
kubectl api-resources --output wide          # All resources with expanded (aka "wide") output
kubectl api-resources --verbs=list,get       # All resources that support the "list" and "get" request verbs
kubectl api-resources --api-group=extensions # All resources in the "extensions" API group
```

Salida: kubectl api-resources --namespaced=true

```Shell
NAME                        SHORTNAMES   APIVERSION                     NAMESPACED   KIND
bindings                                 v1                             true         Binding
configmaps                  cm           v1                             true         ConfigMap
endpoints                   ep           v1                             true         Endpoints
events                      ev           v1                             true         Event
limitranges                 limits       v1                             true         LimitRange
persistentvolumeclaims      pvc          v1                             true         PersistentVolumeClaim
pods                        po           v1                             true         Pod
podtemplates                             v1                             true         PodTemplate
replicationcontrollers      rc           v1                             true         ReplicationController
resourcequotas              quota        v1                             true         ResourceQuota
secrets                                  v1                             true         Secret
serviceaccounts             sa           v1                             true         ServiceAccount
services                    svc          v1                             true         Service
controllerrevisions                      apps/v1                        true         ControllerRevision
daemonsets                  ds           apps/v1                        true         DaemonSet
deployments                 deploy       apps/v1                        true         Deployment
replicasets                 rs           apps/v1                        true         ReplicaSet
statefulsets                sts          apps/v1                        true         StatefulSet
localsubjectaccessreviews                authorization.k8s.io/v1        true         LocalSubjectAccessReview
horizontalpodautoscalers    hpa          autoscaling/v2                 true         HorizontalPodAutoscaler
cronjobs                    cj           batch/v1                       true         CronJob
jobs                                     batch/v1                       true         Job
leases                                   coordination.k8s.io/v1         true         Lease
endpointslices                           discovery.k8s.io/v1            true         EndpointSlice
events                      ev           events.k8s.io/v1               true         Event
ingresses                   ing          networking.k8s.io/v1           true         Ingress
networkpolicies             netpol       networking.k8s.io/v1           true         NetworkPolicy
poddisruptionbudgets        pdb          policy/v1                      true         PodDisruptionBudget
rolebindings                             rbac.authorization.k8s.io/v1   true         RoleBinding
roles                                    rbac.authorization.k8s.io/v1   true         Role
csistoragecapacities                     storage.k8s.io/v1beta1         true         CSIStorageCapacity
```

### ¿A qué tipo de objeto corresponden las siguientes abreviaciones?

- cm:
- ep:
- ns:
- no:
- pv:
- po:
- svc:
- deploy:
- rs:
- ing:
- sc:

## `get`

Para desplegar recursos se tiene el comando `get`

```Shell
kubectl get all
kubectl get all --all-namespaces
kubectl get all --all-namespaces --output=wide
```

Salida: kubectl get all --all-namespaces --output=wide

```Shell
NAMESPACE     NAME                                   READY   STATUS    RESTARTS      AGE   IP             NODE       NOMINATED NODE   READINESS GATES
kube-system   pod/coredns-64897985d-vc54n            1/1     Running   0             91m   172.17.0.2     minikube   <none>           <none>
kube-system   pod/etcd-minikube                      1/1     Running   0             91m   192.168.49.2   minikube   <none>           <none>
kube-system   pod/kube-apiserver-minikube            1/1     Running   0             91m   192.168.49.2   minikube   <none>           <none>
kube-system   pod/kube-controller-manager-minikube   1/1     Running   0             91m   192.168.49.2   minikube   <none>           <none>
kube-system   pod/kube-proxy-nqbs7                   1/1     Running   0             91m   192.168.49.2   minikube   <none>           <none>
kube-system   pod/kube-scheduler-minikube            1/1     Running   0             91m   192.168.49.2   minikube   <none>           <none>
kube-system   pod/storage-provisioner                1/1     Running   1 (91m ago)   91m   192.168.49.2   minikube   <none>           <none>

NAMESPACE     NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)                  AGE   SELECTOR
default       service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP                  91m   <none>
kube-system   service/kube-dns     ClusterIP   10.96.0.10   <none>        53/UDP,53/TCP,9153/TCP   91m   k8s-app=kube-dns

NAMESPACE     NAME                        DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE   CONTAINERS   IMAGES                          SELECTOR
kube-system   daemonset.apps/kube-proxy   1         1         1       1            1           kubernetes.io/os=linux   91m   kube-proxy   k8s.gcr.io/kube-proxy:v1.23.3   k8s-app=kube-proxy

NAMESPACE     NAME                      READY   UP-TO-DATE   AVAILABLE   AGE   CONTAINERS   IMAGES                              SELECTOR
kube-system   deployment.apps/coredns   1/1     1            1           91m   coredns      k8s.gcr.io/coredns/coredns:v1.8.6   k8s-app=kube-dns

NAMESPACE     NAME                                DESIRED   CURRENT   READY   AGE   CONTAINERS   IMAGES                              SELECTOR
kube-system   replicaset.apps/coredns-64897985d   1         1         1       91m   coredns      k8s.gcr.io/coredns/coredns:v1.8.6   k8s-app=kube-dns,pod-template-hash=64897985d
```

## REFERENCIAS

Para conocer más sobre comandos y sus opciones se recomienda visitar la [Referencia de comandos](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#-strong-getting-started-strong-) de `kubectl`.
