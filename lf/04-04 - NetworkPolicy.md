# POLÍTICAS DE RED

Esta actividad muestra como comenzar a usar la API NetworkPolicy de Kubernetes para declarar las políticas de red que rigen la forma en que los pods se comunican entre sí.

## REQUISITOS

A continuación se indican las condiciones necesarias par ala realización de esta actividad.

### Clúster

Para la realización de estas actividades se requiere un proveedor de red compatible con políticas de red, por lo que el primer paso es configurar en el clúster.

### Iniciar Minikube

Inicio minikube con la opción que habilita el plug-in de red:

```Shell
minikube start --network-plugin=cni
```

Salida:

```Shell
😄  minikube v1.25.2 on Ubuntu 20.04 (vbox/amd64)
✨  Automatically selected the docker driver
❗  With --network-plugin=cni, you will need to provide your own CNI. See --cni flag as a user-friendly alternative
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ kubelet.housekeeping-interval=5m
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: storage-provisioner, default-storageclass
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

#### Instalar cilium

Para instalar cilium debe ejecutar los siguientes comandos:

i. Descarga:

```Shell
curl -LO https://github.com/cilium/cilium-cli/releases/latest/download/cilium-linux-amd64.tar.gz
```

Salida:

```Shell
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   154  100   154    0     0    538      0 --:--:-- --:--:-- --:--:--   536
100   664  100   664    0     0   1635      0 --:--:-- --:--:-- --:--:--  1635
100 14.6M  100 14.6M    0     0  9748k      0  0:00:01  0:00:01 --:--:-- 20.5M

```

ii. Desempaquetamiento y eliminación:

```Shell
sudo tar xzvfC cilium-linux-amd64.tar.gz /usr/local/bin
rm cilium-linux-amd64.tar.gz
```

iii. Instalación:

```Shell
cilium install
```

Salida:

```Shell
🔮 Auto-detected Kubernetes kind: minikube
✨ Running "minikube" validation checks
✅ Detected minikube version "1.25.2"
ℹ️  using Cilium version "v1.11.2"
🔮 Auto-detected cluster name: minikube
🔮 Auto-detected IPAM mode: cluster-pool
🔮 Auto-detected datapath mode: tunnel
🔑 Created CA in secret cilium-ca
🔑 Generating certificates for Hubble...
🚀 Creating Service accounts...
🚀 Creating Cluster roles...
🚀 Creating ConfigMap for Cilium version 1.11.2...
🚀 Creating Agent DaemonSet...
🚀 Creating Operator Deployment...
⌛ Waiting for Cilium to be installed and ready...
✅ Cilium was successfully installed! Run 'cilium status' to view installation health
```

iv. Validación de la instalación

Para validar el correcto funcionamiento de Cilium ejecutamos:

```console
cilium status
```

O en el clúster ejecute:

```Shell
kubectl get pods --namespace=kube-system -l k8s-app=cilium
```

Salida:

```Shell
NAME           READY   STATUS    RESTARTS   AGE
cilium-g8f8l   1/1     Running   0          52s
```

Un pod cilium se ejecuta en cada nodo de su clúster y aplica la política de red en el tráfico hacia/desde Pods en ese nodo mediante Linux BPF.

## DESARROLLO

### Creación y Exposición del Deployment

Para ver el funcionamiento de una política de red de Kubernetes, comience creando un _deployment_ de _nginx_.

```Shell
kubectl create deployment nginx --image=nginx
# Salida
# deployment.apps/nginx created
```

Exponga el _deployment_ a través de un servicio llamado **nginx**.

```Shell
kubectl expose deployment nginx --port=80
# Salida
# service/nginx exposed
```

Los comandos anteriores crean un deployment con un pod _nginx_ y lo exponen mediante un servicio llamado **nginx**. El _pod_ y el _deployment_ se encuentran en el espacio de nombres `default`.

```Shell
kubectl get svc,pod
```

Salida:

```none
NAME                        CLUSTER-IP    EXTERNAL-IP   PORT(S)    AGE
service/kubernetes          10.100.0.1    <none>        443/TCP    46m
service/nginx               10.100.0.16   <none>        80/TCP     33s

NAME                        READY         STATUS        RESTARTS   AGE
pod/nginx-701339712-e0qfq   1/1           Running       0          35s
```

### Acceda al Servicio

Se debe poder acceder al nuevo servicio **nginx** desde otros Pods. Para validar dicho acceso inicie un contenedor **busybox**:

```Shell
kubectl run busybox --rm -ti --image=busybox -- /bin/sh
```

En el _shell_ de contenedor (dentro del  _pod_), ejecute el siguiente comando:

```shell
wget --spider --timeout=1 nginx
```

Salida:

```Shell
Connecting to nginx (10.100.0.16:80)
remote file exists
```

### Definición del NetworkPolicy

Para limitar el acceso al servicio **nginx** para que solo los Pods con la etiqueta `access: true` puedan consultarlo, cree un objeto NetworkPolicy de la siguiente manera:

```yaml
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: access-nginx
spec:
  podSelector:
    matchLabels:
      app: nginx
  ingress:
  - from:
    - podSelector:
        matchLabels:
          access: "true"
```

> **Nota:** NetworkPolicy incluye una atributo `podSelector` que selecciona la agrupación de Pods a la que se aplica la política. Puede ver que esta política selecciona Pods con la etiqueta `app=nginx`. La etiqueta se agregó automáticamente al pod en el deployment **nginx**. Un `podSelector` vacío selecciona todos los pods en el espacio de nombres.

### Aplicación del NetworkPolicy

Use `kubectl` para crear una NetworkPolicy a partir de la definición anterior (04_04_NETWORKING_1_network-policy.yaml):

```shell
kubectl apply --filename=04_04_NETWORKING_1_network-policy.yaml --dry-run=server
# Salida
# networkpolicy.networking.k8s.io/access-nginx created
```

### Acceso al Servicio (Sin Etiqueta)

Cuando intenta acceder al servicio **nginx** desde un _pod_ sin las etiquetas correctas, la solicitud no se concluye exitosamente:

```shell
kubectl run busybox --rm -ti --image=busybox -- /bin/sh
```

En el _shell_ de contenedor (dentro del  _pod_), ejecute el siguiente comando:

```shell
wget --spider --timeout=1 nginx
```

Salida:

```none
Connecting to nginx (10.100.0.16:80)
wget: download timed out
```

### Acceso al Servicio (Con Etiqueta)

Puede crear un pod con las etiquetas correctas para ver que la solicitud está permitida:

```Shell
kubectl run busybox-ok --rm -ti --labels="access=true" --image=busybox -- /bin/sh
```

En el _shell_ de contenedor (dentro del  _pod_), ejecute el siguiente comando:

```shell
wget --spider --timeout=1 nginx
```

Salida:

```none
Connecting to nginx (10.100.0.16:80)
remote file exists
```

## OPCIONAL

Visite el repositorio [Kubernetes Network Policy Recipes](https://github.com/ahmetb/kubernetes-network-policy-recipes) y pruebe al menos la siguiente NetworkPolicy:

- LIMIT traffic to an application [enlace](https://github.com/ahmetb/kubernetes-network-policy-recipes/blob/master/02-limit-traffic-to-an-application.md)

![LIMIT traffic to an application](../media/04_04_LAB_01_Recipes.png "LIMIT traffic to an application")

## RESTABLECIMIENTO

### Elementos Creados

Ninguna acción.

### Clúster

Para restablecer el clúster realice los siguientes pasos:

i. Se detiene `minikube`:

```Shell
 minikube stop
```

Salida:

```Shell
✋  Stopping node "minikube"  ...
🛑  Powering off "minikube" via SSH ...
✋  Stopping node "minikube-m02"  ...
🛑  Powering off "minikube-m02" via SSH ...
✋  Stopping node "minikube-m03"  ...
🛑  Powering off "minikube-m03" via SSH ...
🛑  3 nodes stopped.
```

ii. Se elimina el clúster:

```Shell
minikube delete --all
```

Salida:

```Shell
🔥  Deleting "minikube" in docker ...
🔥  Removing /home/netec/.minikube/machines/minikube ...
💀  Removed all traces of the "minikube" cluster.
🔥  Successfully deleted all profiles
```

## REFERENCIAS

- [Declare Network Policy](https://kubernetes.io/docs/tasks/administer-cluster/declare-network-policy/)
- [Deploying Cilium on Minikube for Basic Testing](https://kubernetes.io/docs/tasks/administer-cluster/network-policy-provider/cilium-network-policy/#deploying-cilium-on-minikube-for-basic-testing)
- [cilium](https://cilium.io/)
- [Kubernetes Network Policy Recipes](https://github.com/ahmetb/kubernetes-network-policy-recipes)
