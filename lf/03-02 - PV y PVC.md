# PV y PVC

Esta actividad muestra como configurar un pod para usar un PersistentVolumeClaim para almacenamiento.

Aquí hay un resumen del proceso:

- Como administrador del clúster:
  - Crea un PersistentVolume respaldado por almacenamiento físico
  - No asocia el volumen con ningún pod
- Como desarrollador/usuario del clúster:
  - Crea un PersistentVolumeClaim que se vincula automáticamente a un PersistentVolume
  - Crea un Pod que usa el PersistentVolumeClaim para el almacenamiento

## REQUISITOS

### Clúster

Valide que el clúster se encuentra corriendo:

```Shell
minikube status
```

Salida:

```Shell
minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```

Si la salida es distinta a la anterior, inicie el clúster:

```Shell
minikube start
```

Salida:

```Shell
😄  minikube v1.25.2 on Ubuntu 20.04 (vbox/amd64)
✨  Automatically selected the docker driver
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ kubelet.housekeeping-interval=5m
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: default-storageclass, storage-provisioner
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

## DESARROLLO

### Archivo en el Nodo

Abra un shell en el único nodo de su clúster.

En Minikube ingrese:

```Shell
minikube ssh
# Salida
# docker@minikube:~$
```

En el _shell_ en el nodo, cree el directorio `/mnt/data`:

```shell
# This assumes that your Node uses "sudo" to run commands
# as the superuser
sudo mkdir /mnt/data
```

En el directorio `/mnt/data`, cree un archivo `index.html` como se muestra:

```shell
sudo sh -c "echo 'Hello from Kubernetes storage' > /mnt/data/index.html"
```

Para validar que el archivo se creo con éxito muéstrelo en consola con:

```shell
cat /mnt/data/index.html
```

La salida debe ser:

```Shell
Hello from Kubernetes storage
```

Ahora puede cerrar el shell del nodo con:

```Shell
exit
# Salida
# logout
```

### Creación del PV

Ahora se crea un `PersistentVolume` de _hostPath_. Kubernetes admite hostPath para el desarrollo y las pruebas en un clúster de un solo nodo. Un PersistentVolume hostPath utiliza un archivo o directorio en el nodo para emular el almacenamiento conectado a la red.

> Nota: En un clúster de producción, no usaría hostPath. En su lugar, un administrador de clúster aprovisionaría un recurso de red como un disco persistente de Google Compute Engine, un recurso compartido de NFS o un volumen de Amazon Elastic Block Store.
> Los administradores de clústeres también pueden usar [StorageClasses](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.23/#storageclass-v1-storage) para configurar [el aprovisionamiento dinámico](https://kubernetes.io/blog/2016/10/dynamic-provisioning-and-storage-in-kubernetes).

Analice el archivo de configuración (03_02_PV-PVC_1_pv.yaml) para el `PersistentVolume` de tipo `hostPath`:

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: poc-pv
  labels:
    type: local
    env: poc
spec:
  storageClassName: manual
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/mnt/data"
```

El archivo de configuración especifica que:

- El volumen esta en `/mnt/data` del nodo del clúster
- Un tamaño de 1 gibibytes
- Un modo de acceso de `ReadWriteOnce`
  - Un solo nodo puede montar el volumen como lectura y escritura
- Define el nombre StorageClass `manual` para PersistentVolume, que se usará para vincular las solicitudes de PersistentVolumeClaim a este PersistentVolume

Cree el volumen persistente:

```shell
kubectl apply --filename=03_02_PV-PVC_1_pv.yaml --dry-run=server
# Salida
# persistencevolume/poc-pv created
```

Para ver información sobre PersistentVolume:

```shell
kubectl get PersistentVolume poc-pv
```

El resultado muestra que el PersistentVolume tiene una `STATUS` de `Available`, lo cual significa que aún no se ha vinculado a un PersistentVolumeClaim.

```shell
NAME     CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM   STORAGECLASS   REASON   AGE
poc-pv   1Gi        RWO            Retain           Available           manual                  4m37s
```

### Creación de la PVC

Los pods usan `PersistentVolumeClaims` para solicitar almacenamiento físico. A continuación se creará una PVC que solicita un volumen de al menos 0.5 gibibytes que puede proporcionar acceso de lectura y escritura para al menos un nodo.

Analice el archivo de configuración (03_02_PV-PVC_2_pvc.yaml) para la PVC:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: poc-pvc
  labels:
    env: poc
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 0.5Gi
```

Cree la PersistentVolumeClaim:

```Shell
kubectl apply --filename=03_02_PV-PVC_2_pvc.yaml --dry-run=server
# Salida
# persistentvolumeclaim/poc-pvc created
```

Después de crear `PersistentVolumeClaim`, el plano de control busca un `PersistentVolume` que satisfaga los requisitos de la notificación. Si el plano de control encuentra un PV adecuado con la misma `StorageClass`, vincula la notificación al volumen.

Consulte de nuevo el PersistentVolume:

```shell
kubectl get pv poc-pv
```

Ahora la salida muestra una `STATUS` en `Bound`.

```Shell
NAME     CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM             STORAGECLASS   REASON   AGE
poc-pv   1Gi        RWO            Retain           Bound    default/poc-pvc   manual                  13m
```

Consulte el `PersistentVolumeClaim`:

```shell
kubectl get pvc poc-pvc
```

El resultado muestra que PersistentVolumeClaim está vinculado a su PersistentVolume, `poc-pv`.

```SHELL
NAME      STATUS   VOLUME   CAPACITY   ACCESS MODES   STORAGECLASS   AGE
poc-pvc   Bound    poc-pv   1Gi        RWO            manual         5m34s
```

### Creación del Pod

El siguiente paso es crear un pod que use el `PersistentVolumeClaim` como un volumen.

Analice el archivo de configuración (03_02_PV-PVC_3_pod.yaml) para el pod:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: poc-pvc-pod
  labels:
    env: poc
spec:
  volumes:
    - name: poc-pv-storage
      persistentVolumeClaim:
        claimName: poc-pvc
  containers:
    - name: poc-pvc-container
      image: nginx
      ports:
        - containerPort: 80
          name: "http-server"
      volumeMounts:
        - mountPath: "/usr/share/nginx/html"
          name: poc-pv-storage
```

Tenga en cuenta que el archivo de configuración del pod especifica un `PersistentVolumeClaim`, pero no especifica un `PersistentVolume`. Desde el punto de vista de Pod, la PVC es un volumen.

Cree el pod:

```shell
kubectl apply --filename=03_02_PV-PVC_3_pod.yaml --dry-run=server
# Salida
# pod/poc-pvc-pod created
```

Verifique que el contenedor en el pod se este ejecutando:

```shell
kubectl get pod poc-pvc-pod
```

Salida:

```Shell
NAME          READY   STATUS    RESTARTS   AGE
poc-pvc-pod   1/1     Running   0          119s
```

Obtenga un _shell_ para el contenedor que se ejecuta en su pod:

```shell
kubectl exec -it poc-pvc-pod -- /bin/bash
```

En su shell, verifique que nginx esté sirviendo el archivo `index.html` desde el volumen hostPath:

```shell
# Dentro del contenedor
curl http://localhost/
```

El resultado muestra el texto que escribió en el archivo `index.html` en el volumen `hostPath`:

```Shell
Hello from Kubernetes storage
```

Si ve ese mensaje, ha configurado correctamente un pod para usar el almacenamiento de un `PersistentVolumeClaim`.

## RESTABLECIMIENTO

Para eliminar los elementos creados ejecute:

```shell
kubectl delete pv,pvc,po --selector='env in (poc)'
# Salida
# persistentvolume "poc-pv" deleted
# persistentvolumeclaim "poc-pvc" deleted
# pod "poc-pvc-pod" deleted
```

### Clúster

Para eliminar el archivo y el directorio creado abra un shell en el nodo de su clúster:

Por ejemplo, para Minikube, puede abrir un shell en su nodo ingresando:

```Shell
minikube ssh
# Salida
# docker@minikube:~$
```

En el _shell_ en el nodo, elimine el archivo y el directorio:

```shell
sudo rm /mnt/data/index.html
sudo rmdir /mnt/data
```

Ahora puede cerrar el shell del nodo con:

```Shell
exit
# Salida
# logout
```

## REFERENCIAS

- [Persistence Volume](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)
- [Create a PersistentVolume](https://kubernetes.io/docs/tasks/configure-pod-container/configure-persistent-volume-storage/#create-a-persistentvolume)
