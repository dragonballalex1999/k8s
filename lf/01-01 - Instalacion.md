# 01-01 - Instalación

## REQUISITOS

Se asume que se encuentra instalado lo siguiente:

- Docker
- Terminator
- VSCode

## INSTALACIÓN

En esta sección se instala `kubectl` y _minukube_

### `kubectl`

Para más detalles: [Install and Set Up kubectl on Linux](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)

#### LINUX

Actualizar y configurar los orígenes de repositorios de paquetes

```Shell
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl

sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
```

Actualizar e instalar el paquete requerido

```Shell
sudo apt-get update
sudo apt-get install -y kubectl
```

Validar `kubectl` sin cluster

```Shell
kubectl version --short
kubectl version --client
kubectl cluster-info
```

### Clúster

#### [Minikube](https://minikube.sigs.k8s.io/docs/start/)

##### Instalación

```shell
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube
```

## Inicio/Detención

Desde la terminal de comandos con acceso de administrador, pero no como `root` ejecute:

- Para _levantar_ el clúster:

```shell
minikube start
```

Salida

```shell
netec@n-introk8s:~$ minikube start
😄  minikube v1.25.2 on Ubuntu 20.04 (vbox/amd64)
✨  Automatically selected the docker driver. Other choices: ssh, none
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
💾  Downloading Kubernetes v1.23.3 preload ...
    > preloaded-images-k8s-v17-v1...: 505.68 MiB / 505.68 MiB  100.00% 17.32 Mi
    > gcr.io/k8s-minikube/kicbase: 379.06 MiB / 379.06 MiB  100.00% 10.95 MiB p
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ kubelet.housekeeping-interval=5m
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: storage-provisioner, default-storageclass
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

- Cuando necesite detener el clúster:

```shell
minikube stop
```

Salida

```shell
✋  Stopping node "minikube"  ...
🛑  Powering off "minikube" via SSH ...
🛑  1 node stopped.
```

## Interacción

Mediante `kubectl` se puede obtener información del clúster:

```shell
kubectl cluster-info
```

Salida

```Shell
Kubernetes control plane is running at https://192.168.49.2:8443
CoreDNS is running at https://192.168.49.2:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
```

El siguiente comando lista todos los _pods_ de todos los espacios de nombres:

```shell
#// kubectl get po -A
kubectl get pods --all-namespaces
```

Salida

```Shell
NAMESPACE     NAME                               READY   STATUS    RESTARTS        AGE
kube-system   coredns-64897985d-hzpr8            1/1     Running   0               3m58s
kube-system   etcd-minikube                      1/1     Running   0               4m10s
kube-system   kube-apiserver-minikube            1/1     Running   0               4m12s
kube-system   kube-controller-manager-minikube   1/1     Running   0               4m10s
kube-system   kube-proxy-wx2td                   1/1     Running   0               3m58s
kube-system   kube-scheduler-minikube            1/1     Running   0               4m10s
kube-system   storage-provisioner                1/1     Running   1 (3m28s ago)   4m9s
```

### Desplegar Aplicaciones

Ejecute los siguientes comandos para crear y exponer un _deployment_ en el puerto **8080**:

```Shell
kubectl create deployment hello-deployment --image=k8s.gcr.io/echoserver:1.4
kubectl expose deployment hello-deployment --type=NodePort --port=8080
```

Salida

```Shell
#//kubectl create deployment hello-deployment --image=k8s.gcr.io/echoserver:1.4
deployment.apps/hello-deployment created

#//kubectl expose deployment hello-deployment --type=NodePort --port=8080
service/hello-deployment exposed
```

Después de un momento los objetos desplegados se pueden observar mediante, por ejemplo:

```Shell
kubectl get services hello-deployment
```

Salida

```Shell
NAME               TYPE       CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
hello-deployment   NodePort   10.108.67.44   <none>        8080:30195/TCP   100s
```

Si deseamos consultar el servicio recién desplegado podemos ejecutar:

```Shell
kubectl port-forward service/hello-deployment 7080:8080
```

Salida

```Shell
Forwarding from 127.0.0.1:7080 -> 8080
Forwarding from [::1]:7080 -> 8080
Handling connection for 7080
#// La terminal se queda bloqueada
```

Y abrir el navegador con la url: `localhost:7080`

![Navegador Web: Aplicación](../media/01_01_LAB_01_WB-Webapp.png)

### Comandos básicos en Minikube

A continuación se muestran algunos comandos básicos de minikube.

#### Gestión del clúster

Pausar Kubernetes sin impactar los elementos ya desplegados:

```shell
minikube pause
```

Reanudar, es decir; eliminar la pausa:

```shell
minikube unpause
```

Detener el clúster:

```shell
minikube stop
```

Configurar la memoria asignada (requere reiniciar):

```shell
minikube config set memory 16384
```

Listar el catálogo de _addons_ disponibles:

```shell
minikube addons list
```

Crear un segundo clúster en una versión de Kubernetes específica:

```shell
minikube start -p aged --kubernetes-version=v1.16.1
```

Eliminar todos los clústers de _minikube_:

```shell
minikube delete --all
```

## RESTABLECIMIENTO

En la terminal en que se lanzó el comando que expone el servicio `kubectl port-forward service/hello-deployment 7080:8080` ejecute `Ctrl` + `C` para terminar la ejecución.

A continuación restablezca con `minikube delete --all`
