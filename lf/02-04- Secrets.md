# SECRETS

A continuación se realizaran algunas actividades relacionadas con `Secret`.

## REQUISITOS

### Clúster

Valide que el clúster se encuentra corriendo:

```Shell
minikube status
```

Salida:

```Shell
minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```

Si la salida es distinta a la anterior, inicie el clúster:

```Shell
minikube start
```

Salida:

```Shell
😄  minikube v1.25.2 on Ubuntu 20.04 (vbox/amd64)
✨  Automatically selected the docker driver
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ kubelet.housekeeping-interval=5m
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: default-storageclass, storage-provisioner
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

## DESARROLLO

### `Secrets` con `kubectl`

#### Crear un `Secret`

Una `Secret` puede contener las credenciales de usuario requeridas por los pods para acceder a una base de datos. Por ejemplo, una cadena de conexión de base de datos consta de un nombre de usuario y una contraseña. Puede almacenar el nombre de usuario en un archivo `./username.txt` y la contraseña en un archivo `./password.txt` en su máquina local.

```shell
echo -n 'admin' > ./username.txt
echo -n '1f2d1e2e67df' > ./password.txt
```

En estos comandos, la bandera `-n` garantiza que los archivos generados no tengan un carácter de nueva línea adicional al final del texto. Esto es importante porque cuando `kubectl` lee un archivo y codifica el contenido en una cadena base64, el carácter de nueva línea adicional también se codifica.

El comando `kubectl create secret` empaqueta estos archivos en un `secret` y crea el objeto en el servidor API.

```shell
kubectl create secret generic db-user-pass1 \
  --from-file=./username.txt \
  --from-file=./password.txt
```

La salida es similar a:

```Shell
secret/db-user-pass1 created
```

El nombre de llave predeterminado es el nombre de archivo. Opcionalmente, puede establecer el nombre de la clave usando `--from-file=[key=]source`.

Por ejemplo:

```shell
kubectl create secret generic db-user-pass2 \
  --from-file=username=./username.txt \
  --from-file=password=./password.txt
```

No necesita escapar de los caracteres especiales en las cadenas de contraseña que incluye en un archivo.

También puede proporcionar datos secretos utilizando la etiqueta `--from-literal=<key>=<value>`. Esta etiqueta se puede especificar más de una vez para proporcionar varios pares llave-valor. Tenga en cuenta que los caracteres especiales como `$`, `\`, `*`, `=` y `!` serán interpretados por su shell y requerirán escape.

En la mayoría de los shells, la forma más fácil de escapar de la contraseña es encerrarla entre comillas simples (`'`). Por ejemplo, si su contraseña es `S!B\*d$zDsb=`, ejecute el siguiente comando:

```shell
kubectl create secret generic db-user-pass3 \
  --from-literal=username=devuser \
  --from-literal=password='S!B\*d$zDsb='
```

#### Verificar el `Secret`

Para verificar que el secreto fue creado:

```shell
kubectl get secrets
```

La salida es similar a:

```Shell
NAME                  TYPE                                  DATA   AGE
db-user-pass1         Opaque                                2      90s
db-user-pass2         Opaque                                2      36s
db-user-pass3         Opaque                                2      10s
default-token-prgg9   kubernetes.io/service-account-token   3      30h
```

Puede ver una descripción de `Secret`:

```shell
kubectl describe secrets/db-user-pass2
```

La salida es similar a:

```Shell
Name:         db-user-pass2
Namespace:    default
Labels:       <none>
Annotations:  <none>

Type:  Opaque

Data
====
password:  12 bytes
username:  5 bytes
```

Los comandos `kubectl get` y `kubectl describe` evitan mostrar (de manera predeterminada) el contenido de un archivo `Secret`. Esto es para evitar que el `Secret` se exponga accidentalmente o se almacene en un registro de terminal.

#### Decodificar el `Secret`

Para ver el contenido del secreto, ejecute el siguiente comando:

```shell
kubectl get secret db-user-pass2 --output=jsonpath='{.data}'
```

La salida es similar a:

```json
{"password":"MWYyZDFlMmU2N2Rm","username":"YWRtaW4="}
```

Ahora puede decodificar los datos `password`:

```shell
echo 'MWYyZDFlMmU2N2Rm' | base64 --decode
```

La salida es similar a:

```shell
1f2d1e2e67df
```

Para evitar almacenar un valor codificado secreto en su historial de shell, se puede ejecutar el siguiente comando:

```shell
kubectl get secret db-user-pass2 -o jsonpath='{.data.password}' | base64 --decode
# Salida
# 1f2d1e2e67df
```

La salida será similar a la anterior.

#### Consumir como Archivos

Escriba un archivo de configuración (denominado 02_04_SECRET_3.yaml) para los objetos `Secret` y `Pod` siguientes:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: poc-secret-3
  labels:
    env: poc
type: Opaque
data:
  username: YWRtaW4=
  password: MWYyZDFlMmU2N2Rm
stringData:
  var01: value01
---
apiVersion: v1
kind: Pod
metadata:
  name: secret-files-pod-poc
  labels:
    env: poc
spec:
  containers:
  - name: secret-files-container-poc
    image: nginx:alpine
    volumeMounts:
    - name: secret-volume
      mountPath: "/etc/secret-volume"
      readOnly: true
  volumes:
  - name: secret-volume
    secret:
      secretName: poc-secret-3
```

Aplique la configuración mediante el siguiente comando:

```Shell
kubectl apply --filename=02_04_SECRET_3.yaml --dry-run=server
# Salida
# secret/poc-secret-3 configured
# pod/secret-files-pod-poc created
```

Ingrese a una terminal de comandos en el contenedor (`secret-files-container-poc`) del pod creado:

```Shell
kubectl exec -it pod/secret-files-pod-poc -- sh
```

Dentro de la terminal liste el contenido del directorio de montaje `/etc/secret-volume/`:

```Shell
ls -l /etc/secret-volume/
```

Salida:

```Shell
lrwxrwxrwx    1 root     root            15 Mar 14 03:15 password -> ..data/password
lrwxrwxrwx    1 root     root            15 Mar 14 03:15 username -> ..data/username
lrwxrwxrwx    1 root     root            12 Mar 14 03:15 var01 -> ..data/var01
```

Para mostrar el contenido de los archivos ejecute:

```Shell
more /etc/secret-volume/username
# Salida
# admin
more /etc/secret-volume/var01
# Salida
# value01
```

Para salir de la terminal ejecute `exit`.

#### Restablecimiento

Para eliminar los secretos creados:

```shell
kubectl delete secrets --field-selector=type=Opaque
```

Salida

```Shell
secret "db-user-pass1" deleted
secret "db-user-pass2" deleted
secret "db-user-pass3" deleted
secret "poc-secret-3" deleted
```

Para eliminar el pod:

```shell
kubectl delete pod secret-files-pod-poc
# Salida
# pod "secret-files-pod-poc" deleted
```

### `Secrets` desde Archivos

#### Crear el Archivo

Primero se puede crear un secreto en un archivo, en formato JSON o YAML, y luego crear ese objeto. El recurso `Secret` contiene dos mapas: `data` y `stringData`.

El atributo `data` se usa para almacenar datos arbitrarios, codificados usando base64.

El atributo `stringData` se proporciona por comodidad y le permite proporcionar datos secretos como cadenas no codificadas.

Las llaves de `data` y `stringData` deben constar de caracteres alfanuméricos, `-`, `_` o `.`.

Por ejemplo, para almacenar dos cadenas en un secreto usando el atributo  `data`, convierta las cadenas a base64 de la siguiente manera:

```shell
echo -n 'admin' | base64
```

La salida es similar a:

```Shell
YWRtaW4=
```

```shell
echo -n '1f2d1e2e67df' | base64
```

La salida es similar a:

```Shell
MWYyZDFlMmU2N2Rm
```

Escriba un archivo de configuración (denominado 02_04_SECRET_4.yaml) para el objeto `Secret` siguiente:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: poc-secret-4
  labels:
    env: poc
type: Opaque
data:
  username: YWRtaW4=
  password: MWYyZDFlMmU2N2Rm
```

#### Crear el `Secret`

Ahora cree el secreto usando `kubectl apply`:

```shell
kubectl apply --filename=02_04_SECRET_4.yaml
# Salida
# secret/poc-secret-4 created
```

#### Verificar el `Secret`

El atributo `stringData` es un campo de conveniencia de solo escritura. Nunca se emite cuando se recuperan secretos. Por ejemplo, si ejecuta el siguiente comando:

```shell
kubectl get secret poc-secret-4 --output=YAML
```

La salida es similar a:

```yaml
apiVersion: v1
data:
  password: MWYyZDFlMmU2N2Rm
  username: YWRtaW4=
kind: Secret
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"v1","data":{"password":"MWYyZDFlMmU2N2Rm","username":"YWRtaW4="},"kind":"Secret","metadata":{"annotations":{},"labels":{"env":"poc"},"name":"poc-secret-1","namespace":"default"},"type":"Opaque"}
  creationTimestamp: "2022-03-13T23:32:47Z"
  labels:
    env: poc
  name: poc-secret-4
  namespace: default
  resourceVersion: "30735"
  uid: 6f4f2ac7-1f83-4dbf-ab03-b7e9c6ec8b84
type: Opaque
```

Los comandos `kubectl get` y `kubectl describe` evitan mostrar el contenido de un `Secret` por defecto. Esto es para evitar que se exponga accidentalmente a un espectador o se almacene en un registro de terminal. Para verificar el contenido real de los datos codificados, consulte el [secreto de decodificación](https://kubernetes.io/docs/tasks/configmap-secret/managing-secret-using-kubectl/#decoding-secret).

Si se especifica un campo, como `username`, tanto en `data` como en `stringData`, se utiliza el valor de `stringData`.

Por ejemplo, la siguiente definición de secreto:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: poc-secret
type: Opaque
data:
  username: YWRtaW4=
stringData:
  username: administrator
```

Da como resultado el siguiente secreto:

```yaml
apiVersion: v1
data:
  username: YWRtaW5pc3RyYXRvcg==
kind: Secret
metadata:
  creationTimestamp: 2018-11-15T20:46:46Z
  name: poc-secret
  namespace: default
  resourceVersion: "7579"
  uid: 91460ecb-e917-11e8-98f2-025000000001
type: Opaque
```

Donde `YWRtaW5pc3RyYXRvcg==` decodifica a `administrator`.

#### Consumir como Variable de Entorno

Escriba un archivo de configuración (denominado 02_04_SECRET_5.yaml) para los objetos `Secret` y `Pod` siguientes:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: poc-secret-5
  labels:
    env: poc
type: Opaque
data:
  username: YWRtaW4=
  password: MWYyZDFlMmU2N2Rm
stringData:
  var01: value01
---
apiVersion: v1
kind: Pod
metadata:
  name: secret-env-pod-poc-5
  labels:
    env: poc
spec:
  containers:
  - name: secret-env-container-poc
    image: nginx:alpine
    env:
      - name: SECRET_USERNAME
        valueFrom:
          secretKeyRef:
            name: poc-secret-5
            key: username
      - name: SECRET_VAR01
        valueFrom:
          secretKeyRef:
            name: poc-secret-5
            key: var01
  restartPolicy: Always
```

Aplique la configuración mediante el siguiente comando:

```Shell
kubectl apply --filename=02_04_SECRET_5.yaml --dry-run=client
# Salida
# secret/poc-secret-5 configured
# pod/secret-env-pod-poc-5 created
```

Ingrese a una terminal de comandos en el contenedor (`secret-env-container-poc`) del pod creado:

```Shell
kubectl exec -it pod/secret-env-pod-poc-5 -- sh
```

Dentro de la terminal muestre las dos variables de entorno `SECRET_USERNAME` y `SECRET_VAR01`:

```Shell
echo $SECRET_USERNAME, $SECRET_VAR01
# Salida
# admin, value01
```

#### Restablecimiento

Para eliminar el secreto que ha creado:

```shell
kubectl delete secrets --selector='env in (poc)'
```

## RESTABLECIMIENTO

Todos los elementos ya han sido eliminados.

## REFERENCIAS

- [Secrets](https://kubernetes.io/docs/concepts/configuration/secret/)
- [Managing Secrets using kubectl](https://kubernetes.io/docs/tasks/configmap-secret/managing-secret-using-kubectl/)
- [Managing Secrets using Configuration File](https://kubernetes.io/docs/tasks/configmap-secret/managing-secret-using-config-file/)
