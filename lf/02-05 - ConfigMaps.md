# ConfigMaps

A continuación se realizaran algunas actividades relacionadas con `ConfigMaps`.

## REQUISITOS

### Clúster

Valide que el clúster se encuentra corriendo:

```Shell
minikube status
```

Salida:

```Shell
minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```

Si la salida es distinta a la anterior, inicie el clúster:

```Shell
minikube start
```

Salida:

```Shell
😄  minikube v1.25.2 on Ubuntu 20.04 (vbox/amd64)
✨  Automatically selected the docker driver
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ kubelet.housekeeping-interval=5m
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: default-storageclass, storage-provisioner
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

## DESARROLLO

Siga los pasos a continuación para configurar un caché de Redis usando datos almacenados en un ConfigMap.

### Creación ConfigMap

Primero cree un ConfigMap con un bloque de configuración vacío (02_04_CONFIGMAP_1.yaml):

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: poc-redis-config
  labels:
    env: poc
data:
  redis-config: ""
```

Aplique el ConfigMap creado anteriormente:

```shell
kubectl apply --filename=02_04_CONFIGMAP_1.yaml --dry-run=server
# Salida
# configmap/poc-redis-config created
```

### Creación del Pod

Examine el contenido del manifiesto del pod de Redis y tenga en cuenta lo siguiente:

- Un volumen nombrado `config` es creado por `spec.volumes[1]`
- Los atributos `key` y `path` bajo `spec.volumes[1].items[0]` exponen la llave `redis-config` de `example-redis-config` como un archivo con nombre `redis.conf` en el volumen `config`
- Luego, el volumen `config` se monta en `/redis-master` por `spec.containers[0].volumeMounts[1]`

Esto tiene el efecto de exponer los datos `data.redis-config` del ConfigMap `poc-redis-config` como `/redis-master/redis.conf` dentro del pod.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: redis
  labels:
    env: poc
spec:
  containers:
  - name: redis
    image: redis:5.0.4
    command:
      - redis-server
      - "/redis-master/redis.conf"
    env:
    - name: MASTER
      value: "true"
    ports:
    - containerPort: 6379
    resources:
      limits:
        cpu: "0.1"
    volumeMounts:
    - mountPath: /redis-master-data
      name: data
    - mountPath: /redis-master
      name: config
  volumes:
    - name: data
      emptyDir: {}
    - name: config
      configMap:
        name: poc-redis-config
        items:
        - key: redis-config
          path: redis.conf
```

Ahora aplique el manifiesto de pod de Redis:

```shell
kubectl apply --filename=02_04_CONFIGMAP_2_pod_redis.yaml --dry-run=server
# Salida
# pod/redis created
```

### Análisis de los Objetos Creados

Examine los objetos creados:

```shell
kubectl get pod/redis configmap/poc-redis-config 
```

Debería ver el siguiente resultado:

```shell
NAME        READY   STATUS    RESTARTS   AGE
pod/redis   1/1     Running   0          32s

NAME                         DATA   AGE
configmap/poc-redis-config   1      10m
```

Recuerde que la llave `redis-config` en el ConfigMap `poc-redis-config` esta en blanco:

```shell
kubectl describe configmap/poc-redis-config
```

Se debe ver una llave `redis-config` vacía:

```shell
Name:         poc-redis-config
Namespace:    default
Labels:       env=poc
Annotations:  <none>

Data
====
redis-config:
----


BinaryData
====

Events:  <none>
```

### Configuración Inicial

Use `kubectl exec` para ingresar al pod y ejecutar la herramienta `redis-cli` para verificar la configuración actual:

```shell
kubectl exec -it redis -- redis-cli
```

Solicite la memoria máxima configurada `maxmemory`:

```shell
127.0.0.1:6379> CONFIG GET maxmemory
```

Debería mostrar el valor predeterminado de 0:

```shell
1) "maxmemory"
2) "0"
```

Del mismo modo, verifique `maxmemory-policy`:

```shell
127.0.0.1:6379> CONFIG GET maxmemory-policy
```

Que también debería producir su valor predeterminado de `noeviction`:

```shell
1) "maxmemory-policy"
2) "noeviction"
```

Salga de la terminal con `exit`.

### Actualización del ConfigMap

Ahora adicione algunos valores de configuración al ConfigMap `poc-redis-config`:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: poc-redis-config
  labels:
    env: poc
data:
  redis-config:  |
    maxmemory 2mb
    maxmemory-policy allkeys-lru   
```

Aplique el ConfigMap actualizado:

```shell
kubectl apply --filename=02_04_CONFIGMAP_3.yaml --dry-run=server
# Salida
# configmap/poc-redis-config created
```

Confirme que el ConfigMap se actualizó:

```shell
kubectl describe configmap/poc-redis-config
```

Debería ver los valores de configuración que acabamos de agregar:

```shell
Name:         poc-redis-config
Namespace:    default
Labels:       env=poc
Annotations:  <none>

Data
====
redis-config:
----
maxmemory 2mb
maxmemory-policy allkeys-lru


BinaryData
====

Events:  <none>
```

### Actualización de la Configuración

Verifique el pod `redis` nuevamente usando `redis-cli` via `kubectl exec` para ver si se aplicó la configuración:

```shell
kubectl exec -it redis -- redis-cli
```

Comprobar `maxmemory`:

```shell
127.0.0.1:6379> CONFIG GET maxmemory
```

Se mantiene en el valor predeterminado de 0:

```shell
1) "maxmemory"
2) "0"
```

Del mismo modo, `maxmemory-policy` permanece en la `noeviction` configuración predeterminada:

```shell
127.0.0.1:6379> CONFIG GET maxmemory-policy
```

Salida:

```shell
1) "maxmemory-policy"
2) "noeviction"
```

Los valores de configuración no han cambiado porque el Pod debe reiniciarse para obtener los valores actualizados de los ConfigMaps asociados.

Eliminemos y volvamos a crear el Pod:

```shell
kubectl delete --filename=02_04_CONFIGMAP_2_pod_redis.yaml --dry-run=server
# Salida
# pod "redis" deleted
kubectl apply --filename=02_04_CONFIGMAP_2_pod_redis.yaml --dry-run=server
# Salida
# pod/redis created
```

Ahora vuelva a comprobar los valores de configuración por última vez:

```shell
kubectl exec -it redis -- redis-cli
```

comprobar `maxmemory`:

```shell
127.0.0.1:6379> CONFIG GET maxmemory
```

Ahora debería devolver el valor actualizado de 2097152:

```shell
1) "maxmemory"
2) "2097152"
```

Del mismo modo, `maxmemory-policy` también se ha actualizado:

```shell
127.0.0.1:6379> CONFIG GET maxmemory-policy
```

Ahora refleja el valor deseado de `allkeys-lru`:

```shell
1) "maxmemory-policy"
2) "allkeys-lru"
```

## RESTABLECIMIENTO

Limpie su trabajo eliminando los recursos creados:

```shell
kubectl delete pod/redis configmap/poc-redis-config
```

## REFERENCIAS

- [ConfigMap](https://kubernetes.io/docs/concepts/configuration/configmap/)
- [Configuring Redis using a ConfigMap](https://kubernetes.io/docs/tutorials/configuration/configure-redis-using-configmap/)
