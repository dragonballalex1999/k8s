# NAMESPACES

Esta actividad muestra como ver, trabajar y eliminar espacios de nombres asi como utilizar los espacios de nombres de Kubernetes para subdividir su clúster.

## REQUISITOS

### Clúster

Valide que el clúster se encuentra corriendo:

```Shell
minikube status
```

Salida:

```Shell
minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```

Si la salida es distinta a la anterior, inicie el clúster:

```Shell
minikube start
```

Salida:

```Shell
😄  minikube v1.25.2 on Ubuntu 20.04 (vbox/amd64)
✨  Automatically selected the docker driver
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ kubelet.housekeeping-interval=5m
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: default-storageclass, storage-provisioner
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

## DESARROLLO

### Consulta

Enumere los espacios de nombres actuales en un clúster usando:

```shell
kubectl get namespaces
```

Salida

```Shell
NAME              STATUS   AGE
default           Active   2d23h
kube-node-lease   Active   2d23h
kube-public       Active   2d23h
kube-system       Active   2d23h
```

Kubernetes comienza con tres espacios de nombres iniciales:

- `default`
  - Predeterminado para objetos sin otro espacio de nombres
- `kube-system`
  - Para los objetos creados por el sistema Kubernetes
- `kube-public`
  - Se crea automáticamente y todos los usuarios (incluidos los no autenticados) pueden leerlo
  - Se reserva principalmente para el uso del clúster, en caso de que algunos recursos deban ser visibles y legibles públicamente en todo el clúster.

También puede obtener el resumen de un espacio de nombres específico usando:

```shell
kubectl get namespaces <name>
```

Salida

```Shell
NAME      STATUS   AGE
default   Active   5m5s
```

O puede obtener información detallada con:

```shell
kubectl describe namespaces <name>
```

```Shell
Name:           default
Labels:         <none>
Annotations:    <none>
Status:         Active

No resource quota.

Resource Limits
 Type       Resource    Min Max Default
 ----               --------    --- --- ---
 Container          cpu         -   -   100m
```

Tenga en cuenta que estos detalles muestran tanto la cuota de recursos (si existe) como los rangos de límite de recursos.

La cuota de recursos realiza un seguimiento del uso agregado de los recursos en el espacio de nombres y permite a los operadores de clústeres definir límites estrictos de uso de recursos que puede consumir un espacio de nombres.

Un rango de límite define restricciones mínimas/máximas sobre la cantidad de recursos que una sola entidad puede consumir en un espacio de nombres.

Un espacio de nombres puede estar en una de dos fases:

- `Active`: el espacio de nombres está en uso
- `Terminating`: el espacio de nombres se está eliminando y no se puede usar para nuevos objetos

### Creación

Evite crear un espacio de nombres con el prefijo `kube-`, ya que está reservado para los espacios de nombres del sistema Kubernetes.

Cree un nuevo archivo YAML llamado `05_01_NS_1_ns.yaml`con el contenido:

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: ns-poc
  labels:
    env: poc
```

Entonces ejecute:

```Shell
kubectl create --filename=05_01_NS_1_ns.yaml --dry-run=server
```

Salida

```Shell
namespace/ns-poc created
```

- Alternativamente, puede crear un espacio de nombres usando el siguiente comando:
  `kubectl create namespace ns-poc`

El nombre de su espacio de nombres debe ser una etiqueta DNS válida.

Hay un campo opcional `finalizers`, que permite que los observables purguen los recursos siempre que se elimine el espacio de nombres. Tenga en cuenta que si especifica un finalizador inexistente, el espacio de nombres se creará pero se quedará atascado en el estado `Terminating` si el usuario intenta eliminarlo.

## Eliminación

Al eliminar un espacio de nombres se elimina too lo que está ubicado en el espacio de nombres. Además es importante mencionar que el borrado es asíncrono, por lo que estará en estado `Terminating` un periodo de tiempo que dependerá de la cantidad de elementos que contenga.

```shell
kubectl delete namespaces ns-poc
```

Salida

```Shell
namespace "ns-poc" deleted
```

### Segmentación de un Clúster

Para esta actividad, se crean dos espacios de nombres para nuestro contenido.

En un escenario en el que una organización utiliza un clúster compartido para casos de uso de desarrollo y producción:

- Desarrollo
  - Mantener un espacio en el clúster donde puedan ver la lista de pods, servicios e implementaciones que usan para crear y ejecutar su aplicación
  - En este espacio, los recursos de Kubernetes van y vienen, y las restricciones sobre quién puede o no modificar los recursos se relajan para permitir un desarrollo ágil
- Producción
  - Mantener un espacio en el clúster donde puedan aplicar procedimientos estrictos sobre quién puede o no manipular el conjunto de pods, servicios e `deployments` que ejecutan el sitio de producción

Un patrón que podría seguir esta organización es dividir el clúster en dos espacios de nombres: `development` y `production`.

Cree el espacio de nombres `development` usando kubectl:

```shell
kubectl create -f https://k8s.io/examples/admin/namespace-dev.json
```

Salida:

```Shell
namespace/development created
```

Ahora el espacio de nombres `production` usando kubectl:

```shell
kubectl create -f https://k8s.io/examples/admin/namespace-prod.json
```

Salida:

```Shell
namespace/development created
```

Para asegurarse de que todo esté bien, enumere todos los espacios de nombres en nuestro clúster.

```shell
kubectl get namespaces --show-labels
```

Salida:

```
NAME              STATUS   AGE     LABELS
default           Active   31m     kubernetes.io/metadata.name=default
development       Active   2m57s   kubernetes.io/metadata.name=development,name=development
kube-node-lease   Active   31m     kubernetes.io/metadata.name=kube-node-lease
kube-public       Active   31m     kubernetes.io/metadata.name=kube-public
kube-system       Active   31m     kubernetes.io/metadata.name=kube-system
production        Active   67s     kubernetes.io/metadata.name=production,name=production
```

### Crear Elementos en los Espacios de Nombres

Un espacio de nombres de Kubernetes proporciona el alcance para pods, servicios e `deployments` en el clúster.

Los usuarios que interactúan con un espacio de nombres no ven el contenido de otro espacio de nombres.

#### `development`

Realice las acciones  en el espacio de nombres `development`:

```shell
kubectl create deployment ns-poc --image=k8s.gcr.io/serve_hostname  -n=development --replicas=2
```

Salida:

```Shell
deployment.apps/ns-poc created
```

Hemos creado un `deployment` cuyo tamaño de réplicas es 2 que ejecuta el pod llamado `ns-poc` con un contenedor básico que sirve el nombre de host.

```shell
kubectl get deployments,replicasets,pods --namespace=development
```

Salida:

```Shell
NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/ns-poc   2/2     2            2           2m9s

NAME                                DESIRED   CURRENT   READY   AGE
replicaset.apps/ns-poc-7795c46948   2         2         2       2m9s

NAME                          READY   STATUS    RESTARTS   AGE
pod/ns-poc-7795c46948-4khz5   1/1     Running   0          2m9s
pod/ns-poc-7795c46948-dqb96   1/1     Running   0          2m9s
```

Los desarrolladores pueden hacer lo que requieran y no afectan el contenido en el espacio de nombres `production`.

#### `production`

Cambiemos al espacio de nombres `production` y mostremos como los recursos en un espacio de nombres están ocultos del otro.

```shell
kubectl get deployment,replicasets,pods --namespace=production
```

Salida:

```Shell
No resources found in production namespace.
```

Ahora creemos algunos elementos en este espacio de nombres:

```shell
kubectl create deployment ns-poc --image=k8s.gcr.io/serve_hostname --namespace=production
```

Salida:

```Shell
deployment.apps/ns-poc created
```

Apliquemos escalamiento al `deployment`

```shell
kubectl scale deployment ns-poc --replicas=5 --namespace=production
```

Salida:

```Shell
deployment.apps/ns-poc scaled
```

Ahora mostremos el `deployment`

```shell
kubectl get deployment --namespace=production
```

Salida:

```Shell
NAME     READY   UP-TO-DATE   AVAILABLE   AGE
ns-poc   5/5     5            5           3m38s
```

Ahora consultemos los pods:

```shell
kubectl get pods --selector=app=ns-poc --namespace=production
```

Salida:

```Shell
NAME                      READY   STATUS    RESTARTS   AGE
ns-poc-6f7b787bc4-7h87v   1/1     Running   0          6m21s
ns-poc-6f7b787bc4-8qcz4   1/1     Running   0          6m21s
ns-poc-6f7b787bc4-lhntv   1/1     Running   0          7m45s
ns-poc-6f7b787bc4-lqh8n   1/1     Running   0          6m21s
ns-poc-6f7b787bc4-qpfqm   1/1     Running   0          6m21s
```

## RESTABLECIMIENTO

Para eliminar los elementos creados ejecute:

```Shell
kubectl delete namespace production
# Salida
# namespace "production" deleted
```

```Shell
kubectl delete namespace development
# Salida
# namespace "development" deleted
```

PAra validar que los elementos contenidos en los espacios de nombres han sido eliminados ejecute:

```shell
kubectl get pods --selector=app=ns-poc --namespace=production
# Salida
# No resources found in production namespace.
```

Lo mismo ha pasado en el espacio de nombres `development`.

## REFERENCIAS

- [Namespaces](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/)
- [Share a Cluster with Namespaces](https://kubernetes.io/docs/tasks/administer-cluster/namespaces/)