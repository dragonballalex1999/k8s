# EPHEMERAL VOLUMES

En esta actividad se muestra como configurar un pod para usar un volumen efímero.

## REQUISITOS

### Clúster

Valide que el clúster se encuentra corriendo:

```Shell
minikube status
```

Salida:

```Shell
minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```

Si la salida es distinta a la anterior, inicie el clúster:

```Shell
minikube start
```

Salida:

```Shell
😄  minikube v1.25.2 on Ubuntu 20.04 (vbox/amd64)
✨  Automatically selected the docker driver
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ kubelet.housekeeping-interval=5m
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: default-storageclass, storage-provisioner
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

## DESARROLLO

### Creación del Pod

En este ejercicio se crea un pod que ejecuta un único contenedor. Este pod tiene un volumen efímero que existe durante todo el ciclo de vida del pod, incluso cuando el contenedor es destruido y reiniciado.

Es importante mencionar que, como subordinados del pod se crea una `PersistentVolumeClaim` y un `PersistentValue`.

Analice el archivo de configuración (03_05_EPHEMERAL_1_pod.yaml) para el pod con un volumen efímero:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: poc-ev-pod
  labels:
    env: poc
spec:
  containers:
  - name: poc-ev-container
    image: redis
    volumeMounts:
    - name: redis-storage-ev
      mountPath: /data/redis
  volumes:
    - name: redis-storage-ev
      ephemeral:
        volumeClaimTemplate:
          metadata:
            labels:
              env: poc
          spec:
            accessModes: [ "ReadWriteOnce" ]
            storageClassName: "standard"
            resources:
              requests:
                storage: 1Gi
```

Cree el Pod:

```shell
kubectl apply --filename=03_05_EPHEMERAL_1_pod.yaml --dry-run=server 
# Salida
# pod/poc-ev-pod created
```

Verifique que el contenedor del pod se está ejecutando y después observa los cambios en el Pod

```shell
kubectl get pod poc-ev-pod --watch
```

La salida debería ser similar a:

```shell
NAME         READY   STATUS    RESTARTS   AGE
poc-ev-pod   1/1     Running   0          48s
```

### Consulta de los Elementos

En otra terminal verifique los elementos creados a partir del pod:

```shell
kubectl get po,pv,pvc
```

La salida debería ser similar a:

```shell
NAME             READY   STATUS              RESTARTS   AGE
pod/poc-ev-pod   1/1     Running             0          3s

NAME                                                        CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                                 STORAGECLASS   REASON   AGE
persistentvolume/pvc-d64172b6-436c-4419-829f-daa65596f68e   1Gi        RWO            Delete           Bound    default/poc-ev-pod-redis-storage-ev   standard                3s

NAME                                                STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
persistentvolumeclaim/poc-ev-pod-redis-storage-ev   Bound    pvc-d64172b6-436c-4419-829f-daa65596f68e   1Gi        RWO            standard       3s
```

Como se aprecia en la salida se ha creado un PVC y un PV los cuales ya están asociados y el espacio disponible para el pod.

### Creación Archivo en Volumen Efímero

En otro terminal, abre una sesión interactiva dentro del contenedor que se está ejecutando:

```shell
kubectl exec -it poc-ev-pod -- /bin/bash
```

En el terminal, vaya a `/data/redis` y cree un archivo:

```shell
root@poc-ev-pod:/data# cd /data/redis/
root@poc-ev-pod:/data/redis# echo Hello > test-file
```

### Salida Forzada

En el terminal, liste los procesos en ejecución:

```shell
root@poc-ev-pod:/data/redis# apt-get update
root@poc-ev-pod:/data/redis# apt-get install procps
root@poc-ev-pod:/data/redis# ps aux
```

La salida debería ser similar a:

```shell
root@poc-ev-pod:/data/redis# ps aux
USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
redis          1  0.1  0.0  52828  7252 ?        Ssl  21:13   0:00 redis-server *:6379
root          23  0.0  0.0   4100  3468 pts/0    Ss   21:15   0:00 /bin/bash
root         356  0.0  0.0   6700  2896 pts/0    R+   21:17   0:00 ps aux
```

En el terminal, elimine el proceso de _redis_ donde `<pid>` es el ID de proceso (PID) de Redis.

```shell
root@poc-ev-pod:/data/redis# kill <pid>
```

Vaya al terminal original que observa al pod, eventualmente se verá algo como lo siguiente:

```shell
NAME         READY   STATUS    RESTARTS   AGE
poc-ev-pod   1/1     Running   0          48s
poc-ev-pod   0/1     Completed   0          8m
poc-ev-pod   1/1     Running     1 (8s ago)   8m8s
```

En este punto, el contenedor ha sido destruido y reiniciado. Esto es debido a que el pod de Redis tiene una [restartPolicy](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.23/#podspec-v1-core) de `Always`.

### Validación de Existencia del Archivo

Abra un terminal en el contenedor reiniciado:

```shell
kubectl exec -it poc-ev-pod -- /bin/bash
```

En el terminal, vaya a `/data/redis` y verifique que `test-file` todavía existe:

```shell
root@poc-ev-pod:/data/redis# ls /data/redis/
# Salida
# test-file
```

Salga del terminal con:

```Shell
root@poc-ev-pod:/data/redis# exit
# Salida
# exit
```

### Eliminación/Recreación del pod

Elimine el pod y vuelva a crearlo:

- ¿Aún existe el archivo `/data/redis/test-file`?

## RESTABLECIMIENTO

En la terminal que se lanzó el comando para observar al pod digite: `Ctrl` + `C`

Ahora elimine el pod que se ha creado:

```shell
kubectl delete pod poc-ev-pod
# Salida
# pod "poc-ev-pod" deleted
```

Es importante notar que al realizar la consulta siguiente el resultado nos indica que los elementos dependientes se han eliminado:

```shell
kubectl get po,pv,pvc
# Salida
# No resources found
```

## REFERENCIAS

- [Ephemeral Volumes](https://kubernetes.io/docs/concepts/storage/ephemeral-volumes/)
