# CRONJOB

En esta actividad se construye un `CronJob` que realiza alguna tarea e imprime su resultado.

## REQUISITOS

### Clúster

Valide que el clúster se encuentra corriendo:

```Shell
minikube status
```

Salida:

```Shell
minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```

Si la salida es distinta a la anterior, inicie el clúster:

```Shell
minikube start
```

Salida:

```Shell
😄  minikube v1.25.2 on Ubuntu 20.04 (vbox/amd64)
✨  Automatically selected the docker driver
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ kubelet.housekeeping-interval=5m
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: default-storageclass, storage-provisioner
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

## DESARROLLO

### Creación del CronJob

Examine el contenido del manifiesto del `CronJob` siguiente:

```yaml
apiVersion: batch/v1
kind: CronJob
metadata:
  name: poc-cronjob
  labels:
    env: poc
spec:
  schedule: "* * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: hello
            image: busybox
            imagePullPolicy: IfNotPresent
            command:
            - /bin/sh
            - -c
            - date; echo Hello from the Kubernetes cluster
          restartPolicy: OnFailure
```

Cree un archivo YAML (02_10_CRONJOB_1_cj.yaml) con la definición anterior y mande los cambios al Servidor API:

```Shell
kubectl apply --filename=02_10_CRONJOB_1_cj.yaml --dry-run=server
# Salida
# cronjob.batch/poc-cronjob created
```

### Consulta Elementos

El CronJob anterior ha creado varios elementos: Jobs y Pods.

Para observar información de los Jobs y pods ejecute:

```SHELL
kubectl get CronJobs,Jobs,Pods
```

Salida:

```SHELL
NAME                        SCHEDULE    SUSPEND   ACTIVE   LAST SCHEDULE   AGE
cronjob.batch/poc-cronjob   * * * * *   False     0        30s             3m32s

NAME                             COMPLETIONS   DURATION   AGE
job.batch/poc-cronjob-27455370   1/1           1s         2m30s
job.batch/poc-cronjob-27455371   1/1           1s         90s
job.batch/poc-cronjob-27455372   1/1           2s         30s

NAME                             READY   STATUS      RESTARTS   AGE
pod/poc-cronjob-27455370-x5864   0/1     Completed   0          2m30s
pod/poc-cronjob-27455371-b5dqh   0/1     Completed   0          90s
pod/poc-cronjob-27455372-gzlc9   0/1     Completed   0          30s
```

### Detalles

Para observar el comportamiento de los jobs ejecute:

```shell
kubectl get jobs --watch
```

Después de esperar unos minutos la salida es similar a:

```Shell
poc-cronjob-27455377   1/1           1s         2m37s
poc-cronjob-27455378   1/1           0s         97s
poc-cronjob-27455379   1/1           1s         37s
poc-cronjob-27455380   0/1                      0s
poc-cronjob-27455380   0/1           0s         0s
poc-cronjob-27455380   0/1           1s         1s
poc-cronjob-27455380   1/1           1s         1s
poc-cronjob-27455377   1/1           1s         3m1s
```

Ahora ha visto algunos jobs en ejecución programado por el _CronJob_ `poc-cronjob` cancele el comando anterior con `Ctrl`+ `C` y consulte el _CronJob_:

```shell
kubectl get cronjob poc-cronjob
```

La salida es similar a esto:

```Shell
NAME          SCHEDULE    SUSPEND   ACTIVE   LAST SCHEDULE   AGE
poc-cronjob   * * * * *   False     1        0s              17m
```

Debería ver que se programó con éxito un Job a la hora especificada en `LAST SCHEDULE`. Actualmente hay 1 trabajos activo, lo que significa que el trabajo está en ejecución. Cuando la columna `ACTIVE` tiene el valor `0` significa que el Job a terminado.

### Consulta del Pod

Ahora, busque los pods que creó el último job programado y vea la salida estándar de uno de los pods.

**Nota:** El nombre del Job y el nombre del Pod son diferentes.

```shell
# Sustituya "poc-cronjob-27455388" con el nombre del Job en su sistema
pods=$(kubectl get pods --selector=job-name=poc-cronjob-27455388 --output=jsonpath={.items[*].metadata.name})
```

Mostrar registro de grupo:

```shell
kubectl logs $pods
```

La salida es similar a esto:

```Shell
Tue Mar 15 05:48:00 UTC 2022
Hello from the Kubernetes cluster
```

## RESTABLECIMIENTO

Cuando ya no necesite un _CronJob_, puede eliminarlo con `kubectl delete cronjob <cronjob name>`:

```shell
kubectl delete cronjob poc-cronjob
```

Eliminar el CronJob elimina todos los Jobs y Pods que se crearon y evita que se creen trabajos adicionales.

Al ejecutar:

```SHELL
kubectl get cronjobs,jobs,pods
```

Obtendrá la salida:

```SHELL
No resources found in default namespace.
```

## REFERENCIAS

- [CronJob](https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/)
