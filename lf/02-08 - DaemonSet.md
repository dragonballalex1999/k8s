# DAEMONSET

En esta actividad se construye un `DaemonSet` que creara pods en diferentes nodos.

## REQUISITOS

### Clúster

Para la realización de esta actividad es útil tener un clúster con más de un nodo por lo que a continuación lo configuraremos:

#### Eliminación del clúster mono-nodo

Lo primero que debemos realizar es la eliminación del clúster existente, para lo cual primero detenemos el comando `minikube tunnel` con `Ctrl + C` en la terminal de comandos en la que se encuentre. Para después detener el clúster con `minikune stop`.

Ahora se elimina la configuración actual con:

```Shell
minikube delete --all
```

Salida

```Shell
🔥  Deleting "minikube" in docker ...
🔥  Removing /home/netec/.minikube/machines/minikube ...
💀  Removed all traces of the "minikube" cluster.
🔥  Successfully deleted all profiles
```

#### Creación del clúster multi nodo

Ahora se solicita la creación de un cluster con tres nodos, _Control Plane_ y dos nodos de trabajo.

```SHELL
minikube start --nodes 3
```

Salida

```SHELL
😄  minikube v1.25.2 on Ubuntu 20.04 (vbox/amd64)
✨  Automatically selected the docker driver
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ kubelet.housekeeping-interval=5m
    ▪ kubelet.cni-conf-dir=/etc/cni/net.mk
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔗  Configuring CNI (Container Networking Interface) ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: storage-provisioner, default-storageclass

👍  Starting worker node minikube-m02 in cluster minikube
🚜  Pulling base image ...
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🌐  Found network options:
    ▪ NO_PROXY=192.168.49.2
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ env NO_PROXY=192.168.49.2
🔎  Verifying Kubernetes components...

👍  Starting worker node minikube-m03 in cluster minikube
🚜  Pulling base image ...
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🌐  Found network options:
    ▪ NO_PROXY=192.168.49.2,192.168.49.3
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ env NO_PROXY=192.168.49.2
    ▪ env NO_PROXY=192.168.49.2,192.168.49.3
🔎  Verifying Kubernetes components...
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

Después de esperar un poco, mostrar los nodos con:

```SHELL
kubectl get nodes
```

Salida

```SHELL
NAME           STATUS   ROLES                  AGE   VERSION
minikube       Ready    control-plane,master   14m   v1.23.3
minikube-m02   Ready    <none>                 14m   v1.23.3
minikube-m03   Ready    <none>                 14m   v1.23.3
```

## DESARROLLO

### Creación del DaemonSet

Examine el contenido del manifiesto del DaemonSet siguiente:

```YAML
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: poc-ds
  labels:
    env: poc
spec:
  selector:
    matchLabels:
      name: poc-ds
  template:
    metadata:
      labels:
        name: poc-ds
        env: poc
    spec:
      #tolerations:
        # this toleration is to have the daemonset runnable on primary nodes
        # remove it if your primary nodes can't run pods
        #- key: node-role.kubernetes.io/master
        #  operator: Exists
        #  effect: NoSchedule
      containers:
        - name: poc-ds-container
          image: k8s.gcr.io/busybox
          command: [ "sh", "-c"]
          args:
          - while true; do
              echo "This daemon is running in node $NODE_NAME";
              echo -ne '\n';
              echo "Happy deploying...";
              sleep 600;
            done;
          env:
            - name: NODE_NAME
              valueFrom:
                fieldRef:
                  fieldPath: spec.nodeName 
      restartPolicy: Always
```

Cree un archivo YAML (02_08_DAEMON_1_ds.yaml) con la definición anterior y mande los cambios al Servidor API:

```Shell
kubectl apply --filename=02_08_DAEMON_1_ds.yaml --dry-run=server
# Salida
# daemonset.apps/poc-ds created
```

### Consulta Elementos Creados

El DaemonSet anterior ha creado dos pods en dos diferentes nodos del clúster.

Para observar información de los nodos, DaemonSets y pods ejecute:

```SHELL
kubectl get nodes,daemonsets,pods --output=wide
```

Salida:

```SHELL
NAME                STATUS   ROLES                  AGE   VERSION   INTERNAL-IP    EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
node/minikube       Ready    control-plane,master   70m   v1.23.3   192.168.49.2   <none>        Ubuntu 20.04.2 LTS   5.13.0-35-generic   docker://20.10.12
node/minikube-m02   Ready    <none>                 69m   v1.23.3   192.168.49.3   <none>        Ubuntu 20.04.2 LTS   5.13.0-35-generic   docker://20.10.12
node/minikube-m03   Ready    <none>                 69m   v1.23.3   192.168.49.4   <none>        Ubuntu 20.04.2 LTS   5.13.0-35-generic   docker://20.10.12

NAME                    DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE   CONTAINERS         IMAGES               SELECTOR
daemonset.apps/poc-ds   2         2         2       2            2           <none>          6s    poc-ds-container   k8s.gcr.io/busybox   name=poc-ds

NAME               READY   STATUS    RESTARTS   AGE   IP            NODE           NOMINATED NODE   READINESS GATES
pod/poc-ds-6wl2l   1/1     Running   0          6s    10.244.2.13   minikube-m03   <none>           <none>
pod/poc-ds-shks5   1/1     Running   0          6s    10.244.1.12   minikube-m02   <none>           <none>
```

### Bitácora del Pod

Con la información obtenida en el paso anterior, consulte la información del  pod:

```SHELL
kubectl get pod/poc-ds-6wl2l --output=wide
```

Salida:

```SHELL
kubectl get pod/poc-ds-6wl2l --output=wide
NAME           READY   STATUS    RESTARTS   AGE    IP            NODE           NOMINATED NODE   READINESS GATES
poc-ds-6wl2l   1/1     Running   0          110s   10.244.2.13   minikube-m03   <none>           <none>
```

Y ahora obtenga la salida del pod con el comando siguiente:

```SHELL
kubectl logs pod/poc-ds-6wl2l
```

Salida:

```SHELL
This daemon is running in node minikube-m03

Happy deploying...
```

Lo anterior nos muestra que ese pod se encuentra en un nodo específico y que en el otro existe otro pod.

_Repita la consulta de la bitácora del pod ubicado en el otro nodo._

### `tolerations`

Note que el nodo con el _control-plane_ no tiene un pod. Este es el comportamiento predeterminado de Kubernetes.

Si desea adicionar uno debe eliminar los comentarios en la sección de `tolerations` para adicionar una _tolerancia_ que permite la asignación del pod en ese nodo:

```YAML
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: poc-ds
  labels:
    env: poc
spec:
  selector:
    matchLabels:
      name: poc-ds
  template:
    metadata:
      labels:
        name: poc-ds
        env: poc
    spec:
      tolerations:
        # Permite que el DaemonSet se ejecute en nodos principales
        - key: node-role.kubernetes.io/master
          operator: Exists
          effect: NoSchedule
      containers:
        - name: poc-ds-container
          image: k8s.gcr.io/busybox
          command: [ "sh", "-c"]
          args:
          - while true; do
              echo "This daemon is running in node $NODE_NAME";
              echo -ne '\n';
              echo "Happy deploying...";
              sleep 600;
            done;
          env:
            - name: NODE_NAME
              valueFrom:
                fieldRef:
                  fieldPath: spec.nodeName 
      restartPolicy: Always
```

#### Eliminación y Creación

Para evitar alguna asignación del pod incorrecta, elimine y cree el DaemonSet como se indica a continuación:

```YAML
kubectl delete --filename=02_08_DAEMON_1_ds.yaml --dry-run=server
# Salida
# daemonset.apps/poc-ds deleted
kubectl apply --filename=02_08_DAEMON_1_ds.yaml --dry-run=server
# Salida
# daemonset.apps/poc-ds configured
```

#### Consulta Elementos

El DaemonSet ha creado tres pods, uno en cada uno de los tres nodos del clúster.

Para observar información de los nodos, DaemonSets y pods ejecute:

```SHELL
kubectl get nodes,daemonsets,pods --output=wide
```

Salida:

```SHELL
NAME                STATUS   ROLES                  AGE     VERSION   INTERNAL-IP    EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
node/minikube       Ready    control-plane,master   5m15s   v1.23.3   192.168.49.2   <none>        Ubuntu 20.04.2 LTS   5.13.0-35-generic   docker://20.10.12
node/minikube-m02   Ready    <none>                 4m55s   v1.23.3   192.168.49.3   <none>        Ubuntu 20.04.2 LTS   5.13.0-35-generic   docker://20.10.12
node/minikube-m03   Ready    <none>                 4m32s   v1.23.3   192.168.49.4   <none>        Ubuntu 20.04.2 LTS   5.13.0-35-generic   docker://20.10.12

NAME                    DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE     CONTAINERS         IMAGES               SELECTOR
daemonset.apps/poc-ds   3         3         3       3            3           <none>          4m11s   poc-ds-container   k8s.gcr.io/busybox   name=poc-ds

NAME               READY   STATUS    RESTARTS   AGE     IP           NODE           NOMINATED NODE   READINESS GATES
pod/poc-ds-7cbdr   1/1     Running   0          4m11s   10.244.0.3   minikube       <none>           <none>
pod/poc-ds-9wgn2   1/1     Running   0          4m11s   10.244.2.2   minikube-m03   <none>           <none>
pod/poc-ds-vrx2m   1/1     Running   0          4m11s   10.244.1.2   minikube-m02   <none>           <none>
```

### Bitácora

Con la información obtenida en el paso anterior, consulte la información del pod que se encuentra en el nodo `minikube`:

```SHELL
kubectl logs pod/poc-ds-7cbdr
```

Salida

```SHELL
Este daemon esta corriendo en el nodo minikube

Happy deploying...
```

Si lo considera necesario repita la consulta de la bitácora en los otros pods.

## RESTABLECIMIENTO

Para restablecer el ambiente de trabajo realice las siguientes actividades:

- Elimine el DaemonSet creado:

```shell
kubectl delete daemonset poc-ds --dry-run=server
# Salida
# daemonset.apps "poc-ds" deleted
```

- Verifique que el DaemonSet y los pods han sido eliminados:

```SHELL
kubectl get daemonsets,pods --output=wide
# Salida
# No resources found in default namespace.
```

### Clúster

Por cuestiones de desempeño se dará de baja el clúster con los tres nodos, y se regresará al de uno.

- Se detiene `minikube`:

```Shell
 minikube stop
```

Salida:

```Shell
✋  Stopping node "minikube"  ...
🛑  Powering off "minikube" via SSH ...
✋  Stopping node "minikube-m02"  ...
🛑  Powering off "minikube-m02" via SSH ...
✋  Stopping node "minikube-m03"  ...
🛑  Powering off "minikube-m03" via SSH ...
🛑  3 nodes stopped.
```

- Se elimina el clúster multi nodo:

```Shell
minikube delete --all
```

Salida:

```Shell
🔥  Deleting "minikube" in docker ...
🔥  Removing /home/netec/.minikube/machines/minikube ...
🔥  Removing /home/netec/.minikube/machines/minikube-m02 ...
🔥  Removing /home/netec/.minikube/machines/minikube-m03 ...
💀  Removed all traces of the "minikube" cluster.
🔥  Successfully deleted all profiles
```

- Se inicia `minikune` mono nodo:

```Shell
minikube start
```

Salida:

```Shell
😄  minikube v1.25.2 on Ubuntu 20.04 (vbox/amd64)
✨  Automatically selected the docker driver
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ kubelet.housekeeping-interval=5m
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: default-storageclass, storage-provisioner
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

## REFERENCIAS

- [DaemonSet](https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/)
