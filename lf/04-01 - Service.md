# SERVICES

Esta actividad muestra como crear un objeto de servicio que los clientes externos pueden usar para acceder a una aplicación que se ejecuta en un clúster. El servicio proporciona equilibrio de carga para una aplicación que tiene dos instancias en ejecución

## REQUISITOS

### Clúster

Valide que el clúster se encuentra corriendo:

```Shell
minikube status
```

Salida:

```Shell
minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```

Si la salida es distinta a la anterior, inicie el clúster:

```Shell
minikube start
```

Salida:

```Shell
😄  minikube v1.25.2 on Ubuntu 20.04 (vbox/amd64)
✨  Automatically selected the docker driver
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ kubelet.housekeeping-interval=5m
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: default-storageclass, storage-provisioner
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

## DESARROLLO

### Creación del Deployment

Analice el archivo de configuración (04_01_SVC_1_deploy.yaml) para el deployment:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hello-world
  labels:
    env: poc
spec:
  selector:
    matchLabels:
      run: load-balancer-example
  replicas: 2
  template:
    metadata:
      labels:
        run: load-balancer-example
    spec:
      containers:
        - name: hello-world
          image: gcr.io/google-samples/node-hello:1.0
          ports:
            - containerPort: 8080
              protocol: TCP
```

Cree el deployment:

```shell
kubectl apply --filename=04_01_SVC_1_deploy.yaml --dry-run=server
# Salida
# deployment.apps/hello-world created
```

#### Consulta de los Elementos

El comando anterior crea un _Deployment_ y un _ReplicaSet_ asociado. El ReplicaSet tiene dos pods cada uno de los cuales ejecuta la aplicación Hello World.

Para mostrar información sobre el deployment:

```shell
kubectl get deployments hello-world
kubectl describe deployments hello-world
```

Salida:

```Shell
NAME          READY   UP-TO-DATE   AVAILABLE   AGE
hello-world   2/2     2            2           3m18s

---

Name:                   hello-world
Namespace:              default
CreationTimestamp:      Tue, 15 Mar 2022 21:20:14 -0600
Labels:                 env=poc
Annotations:            deployment.kubernetes.io/revision: 1
Selector:               run=load-balancer-example
Replicas:               2 desired | 2 updated | 2 total | 2 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  run=load-balancer-example
  Containers:
   hello-world:
    Image:        gcr.io/google-samples/node-hello:1.0
    Port:         8080/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   hello-world-6cb8988969 (2/2 replicas created)
Events:
  Type    Reason             Age    From                   Message
  ----    ------             ----   ----                   -------
  Normal  ScalingReplicaSet  3m43s  deployment-controller  Scaled up replica set hello-world-6cb8988969 to 2
```

Para mostrar la información sobre el ReplicaSet:

```shell
kubectl get replicasets
kubectl describe replicasets
```

Salida:

```Shell
NAME                     DESIRED   CURRENT   READY   AGE
hello-world-6cb8988969   2         2         2       4m57s

---

Name:           hello-world-6cb8988969
Namespace:      default
Selector:       pod-template-hash=6cb8988969,run=load-balancer-example
Labels:         pod-template-hash=6cb8988969
                run=load-balancer-example
Annotations:    deployment.kubernetes.io/desired-replicas: 2
                deployment.kubernetes.io/max-replicas: 3
                deployment.kubernetes.io/revision: 1
Controlled By:  Deployment/hello-world
Replicas:       2 current / 2 desired
Pods Status:    2 Running / 0 Waiting / 0 Succeeded / 0 Failed
Pod Template:
  Labels:  pod-template-hash=6cb8988969
           run=load-balancer-example
  Containers:
   hello-world:
    Image:        gcr.io/google-samples/node-hello:1.0
    Port:         8080/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Events:
  Type    Reason            Age   From                   Message
  ----    ------            ----  ----                   -------
  Normal  SuccessfulCreate  5m8s  replicaset-controller  Created pod: hello-world-6cb8988969-958gb
  Normal  SuccessfulCreate  5m8s  replicaset-controller  Created pod: hello-world-6cb8988969-m2bt8
```

### Creación del Servicio

Cree un objeto de servicio que exponga el _deployment_:

```shell
kubectl expose deployment hello-world --type=NodePort --name=example-service
# Salida
# service/example-service exposed
```

#### Consulta de los Elementos

Mostrar información sobre el Servicio:

```shell
kubectl describe services example-service
```

La salida es similar a:

```shell
Name:                     example-service
Namespace:                default
Labels:                   env=poc
Annotations:              <none>
Selector:                 run=load-balancer-example
Type:                     NodePort
IP Family Policy:         SingleStack
IP Families:              IPv4
IP:                       10.103.190.56
IPs:                      10.103.190.56
Port:                     <unset>  8080/TCP
TargetPort:               8080/TCP
NodePort:                 <unset>  32490/TCP
Endpoints:                172.17.0.3:8080,172.17.0.4:8080
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>
```

Tome nota del valor de NodePort para el servicio. Por ejemplo, en el resultado anterior, el valor de NodePort es **32490**.

Enumere los pods que ejecutan la aplicación Hello World:

```shell
kubectl get pods --selector="run=load-balancer-example" --output=wide
```

La salida es similar a:

```shell
NAME                           READY   STATUS    RESTARTS   AGE   IP           NODE       NOMINATED NODE   READINESS GATES
hello-world-6cb8988969-958gb   1/1     Running   0          10m   172.17.0.3   minikube   <none>           <none>
hello-world-6cb8988969-m2bt8   1/1     Running   0          10m   172.17.0.4   minikube   <none>           <none>
```

Obtenga la dirección IP pública de uno de sus nodos que ejecuta un pod Hello World. La forma en que obtiene esta dirección depende de cómo configure su clúster.

Por ejemplo, en Minikube, puede ver la dirección del nodo ejecutando `kubectl cluster-info`.

```Shell
kubectl cluster-info
```

Salida:

```Shell
Kubernetes control plane is running at https://192.168.49.2:8443
CoreDNS is running at https://192.168.49.2:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

En este caso la dirección IP del nodo es: **192.168.49.2**

### Consumo del Servicio

Utilice la dirección del nodo y el puerto del nodo para acceder a la aplicación Hello World:

```shell
# curl http://<public-node-ip>:<node-port>
curl http://192.168.49.2:32490
```

Donde:

- `<public-node-ip>`: es la dirección IP pública del nodo
- `<node-port>`: Es el valor de NodePort para el servicio

La respuesta a una solicitud exitosa es un mensaje de saludo:

```shell
Hello Kubernetes!
```

### Archivo de Configuración

Como alternativa al uso `kubectl expose`, puede usar un archivo de configuración para crear el servicio.

#### Borrado de los Elementos

Elimine el servicio (y el endpoint) creado en el paso anterior para que el deployment no pueda ser consumido como en el paso anterior.

Ejecute:

```Shell
kubectl delete service example-service
# Salida
# service "example-service" deleted
```

#### Creación del Servicio

Analice el archivo de configuración (04_01_SVC_2_svc.yaml) para el servicio:

```yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    env: poc
  name: example-service-2
spec:
  ports:
  - nodePort: 32490
    port: 8080
    protocol: TCP
    targetPort: 8080
  selector:
    run: load-balancer-example
  type: NodePort
```

Cree el servicio:

```shell
kubectl apply --filename=04_01_SVC_2_svc.yaml --dry-run=server
# Salida
# service/example-service-2 created
```

#### Consulta de los Elementos

Mostrar información sobre el servicio:

```shell
kubectl describe services example-service-2
```

La salida es similar a:

```shell
Name:                     example-service-2
Namespace:                default
Labels:                   env=poc
Annotations:              <none>
Selector:                 run=load-balancer-example
Type:                     NodePort
IP Family Policy:         SingleStack
IP Families:              IPv4
IP:                       10.103.15.87
IPs:                      10.103.15.87
Port:                     <unset>  8080/TCP
TargetPort:               8080/TCP
NodePort:                 <unset>  32490/TCP
Endpoints:                172.17.0.3:8080,172.17.0.4:8080
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>
```

Tome nota del valor de NodePort para el servicio. Por ejemplo, en el resultado anterior, el valor de NodePort es **32490**.

Obtenga la dirección IP pública de uno de sus nodos que ejecuta un pod Hello World. La forma en que obtiene esta dirección depende de cómo configure su clúster.

Por ejemplo, en Minikube, puede ver la dirección del nodo ejecutando `kubectl cluster-info`.

```Shell
kubectl cluster-info
```

Salida:

```Shell
Kubernetes control plane is running at https://192.168.49.2:8443
CoreDNS is running at https://192.168.49.2:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

En este caso la dirección IP del nodo es: **192.168.49.2**

#### Consumo del Servicio

Utilice la dirección del nodo y el puerto del nodo para acceder a la aplicación Hello World:

```shell
# curl http://<public-node-ip>:<node-port>
curl http://192.168.49.2:32490
```

Donde:

- `<public-node-ip>`: es la dirección IP pública del nodo
- `<node-port>`: Es el valor de NodePort para el servicio

La respuesta a una solicitud exitosa es un mensaje de saludo:

```shell
Hello Kubernetes!
```

## RESTABLECIMIENTO

Para eliminar los elementos creados ejecute:

```shell
kubectl delete svc,deploy --selector='env in (poc)'
# Salida
# service "example-service-2" deleted
# deployment.apps "hello-world" deleted
```

## REFERENCIAS

- [Service](https://kubernetes.io/docs/concepts/services-networking/service/)
- [Use a Service to Access an Application in a Cluster](https://kubernetes.io/docs/tasks/access-application-cluster/service-access-application-cluster/)