# Deployment

Siga los pasos a continuación para ejecutar una aplicación sin estado utilizando un `Deployment`.

## REQUISITOS

### Clúster

Valide que el clúster se encuentra corriendo:

```Shell
minikube status
```

Salida:

```Shell
minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```

Si la salida es distinta a la anterior, inicie el clúster:

```Shell
minikube start
```

Salida:

```Shell
😄  minikube v1.25.2 on Ubuntu 20.04 (vbox/amd64)
✨  Automatically selected the docker driver
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ kubelet.housekeeping-interval=5m
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: default-storageclass, storage-provisioner
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

## DESARROLLO

### Creación `deployment`

Puede escribir un archivo con la configuración que permita desplegar una aplicación, todo esto mediante el `deployment`.

Por ejemplo, el siguiente archivo YAML (02_06_DEPLOY_1_deploy.yaml) describe un `deployment` que usa la imagen Docker `nginx:1.7.9`:

```yaml
apiVersion: apps/v1 # Usa apps/v1beta2 para versiones anteriores a 1.9.0
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    env: poc
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2 # indica al controlador que ejecute 2 pods
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80
```

- Cree un `deployment` basado en el fichero YAML:

```Shell
kubectl apply --filename=02_06_DEPLOY_1_deploy.yaml --dry-run=server
# Salida
# deployment.apps/nginx-deployment created
```

- Obtenga información acerca del `deployment`:

```Shell
kubectl describe deployment nginx-deployment
```

El resultado es similar a:

```Shell
Name:                   nginx-deployment
Namespace:              default
CreationTimestamp:      Mon, 14 Mar 2022 11:52:15 -0600
Labels:                 env=poc
Annotations:            deployment.kubernetes.io/revision: 1
Selector:               app=nginx
Replicas:               2 desired | 2 updated | 2 total | 2 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=nginx
  Containers:
   nginx:
    Image:        nginx:1.7.9
    Port:         80/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   nginx-deployment-f7ccf9478 (2/2 replicas created)
Events:
  Type    Reason             Age   From                   Message
  ----    ------             ----  ----                   -------
  Normal  ScalingReplicaSet  72s   deployment-controller  Scaled up replica set nginx-deployment-f7ccf9478 to 2
```

- Liste los pods creados por el `deployment`:

```Shell
# kubectl get pods -l app=nginx
kubectl get pods --selector='app in (nginx)'
```

El resultado es similar a:

```Shell
NAME                               READY   STATUS    RESTARTS   AGE
nginx-deployment-f7ccf9478-dwfrn   1/1     Running   0          2m57s
nginx-deployment-f7ccf9478-mztxh   1/1     Running   0          2m57s
```

- Muestre información acerca del pod:

```Shell
# kubectl describe pod <pod-name>
kubectl describe pod nginx-deployment-f7ccf9478-dwfrn
```

El resultado es similar a:

```Shell
Name:         nginx-deployment-f7ccf9478-dwfrn
Namespace:    default
Priority:     0
Node:         minikube/192.168.49.2
Start Time:   Mon, 14 Mar 2022 11:52:15 -0600
Labels:       app=nginx
              pod-template-hash=f7ccf9478
Annotations:  <none>
Status:       Running
IP:           172.17.0.4
IPs:
  IP:           172.17.0.4
Controlled By:  ReplicaSet/nginx-deployment-f7ccf9478
Containers:
  nginx:
    Container ID:   docker://c22f521b91df2ccb3b96f4c63999fe352c041a458cd1851091afa7781f610553
    Image:          nginx:1.7.9
    Image ID:       docker-pullable://nginx@sha256:e3456c851a152494c3e4ff5fcc26f240206abac0c9d794affb40e0714846c451
    Port:           80/TCP
    Host Port:      0/TCP
    State:          Running
      Started:      Mon, 14 Mar 2022 11:52:24 -0600
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-czv78 (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  kube-api-access-czv78:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  3m59s  default-scheduler  Successfully assigned default/nginx-deployment-f7ccf9478-dwfrn to minikube
  Normal  Pulling    3m59s  kubelet            Pulling image "nginx:1.7.9"
  Normal  Pulled     3m50s  kubelet            Successfully pulled image "nginx:1.7.9" in 9.015698493s
  Normal  Created    3m50s  kubelet            Created container nginx
  Normal  Started    3m50s  kubelet            Started container nginx
```

### Actualización del `deployment`

Puede actualizar el `deployment` aplicando un nuevo archivo YAML. El siguiente archivo YAML (02_06_DEPLOY_2_deploy_updated_version.yaml) especifica que el `deployment` debería ser actualizado para usar `nginx 1.8`.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 2
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.8 # Actualiza la versión de nginx de 1.7.9 a 1.8
        ports:
        - containerPort: 80
```

- Aplique el nuevo archivo YAML:

```Shell|
kubectl apply --filename=02_06_DEPLOY_2_deploy_updated_version.yaml --dry-run=server
# Salida
# deployment.apps/nginx-deployment configured
```

- Compruebe como el `deployment` crea nuevos pods con la nueva imagen mientras va eliminando los pods con la especificación antigua:

```Shell
# kubectl get pods -l app=nginx
kubectl get pods --selector='app in (nginx)'
```

Salida:

```Shell
NAME                               READY   STATUS    RESTARTS   AGE
nginx-deployment-94c47774b-6mhb6   1/1     Running   0          57s
nginx-deployment-94c47774b-gkndr   1/1     Running   0          66s
```

En la situación de no visualizar la transición, note que los nombres de los pods son distintos a los anteriores, ya que son nuevos.

Adicionalmente puede notar que en la sección de eventos del deployment hace referencia a dos ReplicaSet

```Shell
kubectl describe deployment nginx-deployment
```

El resultado es similar a:

```Shell
Name:                   nginx-deployment
Namespace:              default
CreationTimestamp:      Mon, 14 Mar 2022 11:52:15 -0600
Labels:                 env=poc
Annotations:            deployment.kubernetes.io/revision: 2
Selector:               app=nginx
Replicas:               2 desired | 2 updated | 2 total | 2 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=nginx
  Containers:
   nginx:
    Image:        nginx:1.8
    Port:         80/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   nginx-deployment-94c47774b (2/2 replicas created)
Events:
  Type    Reason             Age    From                   Message
  ----    ------             ----   ----                   -------
  Normal  ScalingReplicaSet  16m    deployment-controller  Scaled up replica set nginx-deployment-f7ccf9478 to 2
  Normal  ScalingReplicaSet  5m13s  deployment-controller  Scaled up replica set nginx-deployment-94c47774b to 1
  Normal  ScalingReplicaSet  5m4s   deployment-controller  Scaled down replica set nginx-deployment-f7ccf9478 to 1
  Normal  ScalingReplicaSet  5m4s   deployment-controller  Scaled up replica set nginx-deployment-94c47774b to 2
  Normal  ScalingReplicaSet  5m3s   deployment-controller  Scaled down replica set nginx-deployment-f7ccf9478 to 0
```

### Escalamiento del `deployment`

Puede aumentar el número de pods en el `deployment` aplicando un nuevo archivo YAML.

El siguiente archivo YAML (02_06_DEPLOY_3_deploy_updated_replicas.yaml) especifica un total de 4 `replicas`, lo que significa que el `deployment` debería tener cuatro pods:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    env: poc
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 4 # Indica al controlador que ejecute 4 pods
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.8
        ports:
        - containerPort: 80
```

- Aplica el nuevo archivo YAML:

```Shell
kubectl apply --filename=02_06_DEPLOY_3_deploy_updated_replicas.yaml --dry-run=server
# Salida
# deployment.apps/nginx-deployment configured
```

- Verifique que el `deployment` tiene cuatro pods:

```Shell
# kubectl get pods -l app=nginx
kubectl get pods --selector='app in (nginx)'
```

El resultado es similar a esto:

```Shell
NAME                               READY   STATUS    RESTARTS   AGE
nginx-deployment-94c47774b-6mhb6   1/1     Running   0          14m
nginx-deployment-94c47774b-8vgpn   1/1     Running   0          7s
nginx-deployment-94c47774b-gkndr   1/1     Running   0          14m
nginx-deployment-94c47774b-jgn2r   1/1     Running   0          8s
```

## RESTABLECIMIENTO

Antes de eliminar el deployment consulte el deployment, los ReplicaSet y los Pods:

```Shell
kubectl get deployments
kubectl get replicasets
kubectl get pods
```

Ahora elimine el `deployment` por el nombre:

```Shell
kubectl delete deployment nginx-deployment
```

Verifique que el deployment, los ReplicaSet y los pods han sido eliminados

```Shell
kubectl get deployments
kubectl get replicasets
kubectl get pods
```

## REFERENCIAS

- [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)
- [Run a Stateless Application Using a Deployment](https://kubernetes.io/docs/tasks/run-application/run-stateless-application-deployment/)
