# VOLUMES

En esta actividad se muestra como configurar un Pod para usar un `Volume` (volumen) como almacenamiento.

El sistema de archivos de un contenedor existe mientras el contenedor exista. Por tanto, cuando es destruido o reiniciado, los cambios realizados en el sistema de archivos se pierden. Para un almacenamiento más consistente que sea independiente del ciclo de vida del contenedor, puede usar un [Volume](https://kubernetes.io/docs/concepts/storage/volumes/). Esta característica es especialmente importante para aplicaciones que deben mantener un estado, como motores de almacenamiento llave-valor (por ejemplo Redis) y bases de datos.

## REQUISITOS

### Clúster

Valide que el clúster se encuentra corriendo:

```Shell
minikube status
```

Salida:

```Shell
minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```

Si la salida es distinta a la anterior, inicie el clúster:

```Shell
minikube start
```

Salida:

```Shell
😄  minikube v1.25.2 on Ubuntu 20.04 (vbox/amd64)
✨  Automatically selected the docker driver
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ kubelet.housekeeping-interval=5m
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: default-storageclass, storage-provisioner
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

## DESARROLLO

### Creación del Pod

En esta actividad se crea un pod que ejecuta un único contenedor. El pod tiene un _Volume_ de tipo [emptyDir](https://kubernetes.io/docs/concepts/storage/volumes/#emptydir) (directorio vacío) que existe durante todo el ciclo de vida del pod, incluso cuando el contenedor es destruido y reiniciado.

Analice el archivo de configuración (03_01_VOLUME_1_pod.yaml) para el pod con un volumen `emptyDir`:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: poc-volume
  labels:
    env: poc
spec:
  containers:
  - name: poc-volume-container
    image: redis
    volumeMounts:
    - name: redis-storage
      mountPath: /data/redis
  volumes:
  - name: redis-storage
    emptyDir: {}
```

Cree el Pod:
    
```shell
kubectl apply --filename=03_01_VOLUME_1_pod.yaml 
# Salida
# pod/poc-volume created
```

Verifique que el contenedor del pod se está ejecutando y después observa los cambios en el Pod

```shell
kubectl get pod poc-volume --watch
```

La salida debería ser similar a:

```shell
NAME         READY   STATUS    RESTARTS   AGE
poc-volume   1/1     Running   0          48s
```

### Creación Archivo en el Pod

En otro terminal, abra una sesión interactiva dentro del contenedor que se está ejecutando:

```shell
kubectl exec -it poc-volume -- /bin/bash
```

En el terminal, vaya a `/data/redis` y cree un archivo:

```shell
root@poc-volume:/data# cd /data/redis/
root@poc-volume:/data/redis# echo Hello > test-file
```

### Salida Forzada

En el terminal, liste los procesos en ejecución:

```shell
root@poc-volume:/data/redis# apt-get update
root@poc-volume:/data/redis# apt-get install procps
root@poc-volume:/data/redis# ps aux
```

La salida debería ser similar a:

```shell
root@poc-volume:/data/redis# ps aux
USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
redis          1  0.1  0.0  52828  7252 ?        Ssl  21:13   0:00 redis-server *:6379
root          23  0.0  0.0   4100  3468 pts/0    Ss   21:15   0:00 /bin/bash
root         356  0.0  0.0   6700  2896 pts/0    R+   21:17   0:00 ps aux
```

En el terminal, elimine el proceso de _redis_ donde `<pid>` es el ID de proceso (PID) de Redis.

```shell
root@poc-volume:/data/redis# kill <pid>
```

En el terminal original, observa los cambios en el pod de Redis. Eventualmente se verá algo como lo siguiente:

```shell
NAME         READY   STATUS    RESTARTS   AGE
poc-volume   1/1     Running   0          48s
poc-volume   0/1     Completed   0          8m
poc-volume   1/1     Running     1 (8s ago)   8m8s
```

En este punto, el contenedor ha sido destruido y reiniciado. Esto es debido a que el pod de Redis tiene una [restartPolicy](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.23/#podspec-v1-core) de `Always`.

### Validación de Existencia del Archivo

Abra un terminal en el contenedor reiniciado:

```shell
kubectl exec -it poc-volume -- /bin/bash
```

En el terminal, vaya a `/data/redis` y verifique que `test-file` todavía existe:

```shell
root@poc-volume:/data/redis# ls /data/redis/
# Salida
# test-file
```

Salga del terminal con:

```Shell
root@poc-volume:/data/redis# exit
# Salida
# exit
```

## RESTABLECIMIENTO

En la terminal que se lanzó el comando para observar al pod digite: `Ctrl` + `C`

Ahora elimine el Pod que se ha creado:

```shell
kubectl delete pod poc-volume
# Salida
# pod "poc-volume" deleted
```

## REFERENCIAS

- [Volumes](https://kubernetes.io/docs/concepts/storage/volumes/)
- [Configure a Pod to Use a Volume for Storage](https://kubernetes.io/docs/tasks/configure-pod-container/configure-volume-storage/)
