# 01-02: Dashboard Minikube

## Iniciar minikube

Desde la terminal de comandos con acceso de administrador, pero no como `root` ejecute:

```shell
minikube start
```

Salida

```shell
netec@n-introk8s:~$ minikube start
😄  minikube v1.25.2 on Ubuntu 20.04 (vbox/amd64)
✨  Automatically selected the docker driver. Other choices: ssh, none
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
💾  Downloading Kubernetes v1.23.3 preload ...
    > preloaded-images-k8s-v17-v1...: 505.68 MiB / 505.68 MiB  100.00% 17.32 Mi
    > gcr.io/k8s-minikube/kicbase: 379.06 MiB / 379.06 MiB  100.00% 10.95 MiB p
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ kubelet.housekeeping-interval=5m
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: storage-provisioner, default-storageclass
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

Minikube provee una gran variedad de herramientas que simplifican el trabajo con Kubernetes, una de ellas es el comando `minikube tunnel` el cual crea una ruta a los servicios implementados con un _LoadBalancer_ y establece su _Ingress_ en su _ClusterIP_.

En otra terminal el comando mostrado en el siguiente comando:

```shell
minikube tunnel
```

Salida

```Shell
Status: 
 machine: minikube
 pid: 468460
 route: 10.96.0.0/12 -> 192.168.49.2
 minikube: Running
 services: [my-app]
    errors: 
  minikube: no errors
  router: no errors
  loadbalancer emulator: no errors
```

La terminal queda bloqueada y muestra detalles de la ejecución. _Cuando requiera salir de este debe terminar el comando con `Ctrl` + `C`_.

## Iniciar Dashboard

```shell
minikube dashboard
```

Salida: El puerto puede ser distinto cada ocasión

```shell
🔌  Enabling dashboard ...
    ▪ Using image kubernetesui/dashboard:v2.3.1
    ▪ Using image kubernetesui/metrics-scraper:v1.0.7
🤔  Verifying dashboard health ...
🚀  Launching proxy ...
🤔  Verifying proxy health ...
🎉  Opening http://127.0.0.1:40565/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/ in your default browser...
```

## Visión General: WEBUI

### Pantalla Inicial

Dedique unos minutos a familiarizarse con la interfaz

- <http://127.0.0.1:40565/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/#/workloads?namespace=default>
  - El puerto puede ser distinto cada ocasión

![Kubernetes Dashboard](../media/01_02_LAB_01_Dashboard-Vacio.png)

### Creación Desde Formulario

Utilice la siguiente información para crear un nuevo recurso desde un formulario:

| Propiedad       | Valor        |
| --------------- | ------------ |
| App name        | my-app       |
| Container image | nginx:alpine |
| Number of pods  | 1            |
| Service         | External     |
| Port            | 8000         |
| Target port     | 80           |
| Protocol        | TCP          |

![Kubernetes Dashboard: Primer Deploy](../media/01_02_LAB_02_Primer-Deploy.png)

### Elementos Creados

Analice el dashboard y determine que elementos se han añadido

1. Deployments
2. Pods
3. ReplicaSets
4. Services

### Navegador Web

El la pantalla principal de los servicios, se puede localizar en la columns _External Endpoints_ la dirección en la que se expone el servicio recién creado.

![Primer Webapp](../media/01_02_LAB_03_Primer-app.png)

## `kubectl`

A continuación se obtiene información de los elementos recién creados mediante la herramienta `kubectl` y los comandos `get` y `explain`.

```shell
kubectl get all
```

Salida

```shell
NAME                         READY   STATUS    RESTARTS   AGE
pod/my-app-68bd7d47b-4qmmm   1/1     Running   0          74m

NAME                 TYPE           CLUSTER-IP     EXTERNAL-IP    PORT(S)          AGE
service/kubernetes   ClusterIP      10.96.0.1      <none>         443/TCP          3h22m
service/my-app       LoadBalancer   10.100.65.38   10.100.65.38   8000:30126/TCP   74m

NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/my-app   1/1     1            1           74m

NAME                               DESIRED   CURRENT   READY   AGE
replicaset.apps/my-app-68bd7d47b   1         1         1       74m
```

### Pod

Los pods son una colección de contenedores que puede correr en un host. Puede ser creado por clientes o por la ejecución _calendarizada_.

#### explain

```shell
kubectl explain pod
kubectl explain pod.kind
kubectl explain pod --recursive
```

Salida

```shell
KIND:     Pod
VERSION:  v1

DESCRIPTION:
     Pod is a collection of containers that can run on a host. This resource is
     created by clients and scheduled onto hosts.

FIELDS:
   apiVersion   <string>
     APIVersion defines the versioned schema of this representation of an
     object. Servers should convert recognized schemas to the latest internal
     value, and may reject unrecognized values. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources

   kind <string>
     Kind is a string value representing the REST resource this object
     represents. Servers may infer this from the endpoint the client submits
     requests to. Cannot be updated. In CamelCase. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds

   metadata     <Object>
     Standard object\'s metadata. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata

   spec <Object>
     Specification of the desired behavior of the pod. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#spec-and-status

   status       <Object>
     Most recently observed status of the pod. This data may not be up to date.
     Populated by the system. Read-only. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#spec-and-status
```

#### get

```shell
kubectl get po
kubectl get pod
kubectl get pods
```

Salida

```shell
NAME                     READY   STATUS    RESTARTS   AGE
my-app-68bd7d47b-4qmmm   1/1     Running   0          77m
```

##### get (resource) (name)

```shell
kubectl get pod my-app-68bd7d47b-4qmmm
kubectl get pod/my-app-68bd7d47b-4qmmm
```

Salida

```shell
NAME                     READY   STATUS    RESTARTS   AGE
my-app-68bd7d47b-4qmmm   1/1     Running   0          78m
```

##### get (resource) (name) (options)

```shell
kubectl get pod my-app-68bd7d47b-4qmmm --output=yaml
```

Salida

```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: "2022-02-27T21:57:37Z"
  generateName: my-app-68bd7d47b-
  labels:
    k8s-app: my-app
    pod-template-hash: 68bd7d47b
  name: my-app-68bd7d47b-4qmmm
  namespace: default
  ownerReferences:
  - apiVersion: apps/v1
    blockOwnerDeletion: true
    controller: true
    kind: ReplicaSet
    name: my-app-68bd7d47b
    uid: 884ef8dc-aad4-41da-bb7c-77fb806a5cbb
  resourceVersion: "4484"
  uid: 8a17e6cb-c77d-4b80-976a-f32ce066b8ff
spec:
  containers:
  - image: nginx:alpine
    imagePullPolicy: IfNotPresent
    name: my-app
    resources: {}
    securityContext:
      privileged: false
    terminationMessagePath: /dev/termination-log
    terminationMessagePolicy: File
    volumeMounts:
    - mountPath: /var/run/secrets/kubernetes.io/serviceaccount
      name: kube-api-access-dvk9s
      readOnly: true
  dnsPolicy: ClusterFirst
  enableServiceLinks: true
  nodeName: minikube
  preemptionPolicy: PreemptLowerPriority
  priority: 0
  restartPolicy: Always
  schedulerName: default-scheduler
  securityContext: {}
  serviceAccount: default
  serviceAccountName: default
  terminationGracePeriodSeconds: 30
  tolerations:
  - effect: NoExecute
    key: node.kubernetes.io/not-ready
    operator: Exists
    tolerationSeconds: 300
  - effect: NoExecute
    key: node.kubernetes.io/unreachable
    operator: Exists
    tolerationSeconds: 300
  volumes:
  - name: kube-api-access-dvk9s
    projected:
      defaultMode: 420
      sources:
      - serviceAccountToken:
          expirationSeconds: 3607
          path: token
      - configMap:
          items:
          - key: ca.crt
            path: ca.crt
          name: kube-root-ca.crt
      - downwardAPI:
          items:
          - fieldRef:
              apiVersion: v1
              fieldPath: metadata.namespace
            path: namespace
status:
  conditions:
  - lastProbeTime: null
    lastTransitionTime: "2022-02-27T21:57:37Z"
    status: "True"
    type: Initialized
  - lastProbeTime: null
    lastTransitionTime: "2022-02-27T21:57:39Z"
    status: "True"
    type: Ready
  - lastProbeTime: null
    lastTransitionTime: "2022-02-27T21:57:39Z"
    status: "True"
    type: ContainersReady
  - lastProbeTime: null
    lastTransitionTime: "2022-02-27T21:57:37Z"
    status: "True"
    type: PodScheduled
  containerStatuses:
  - containerID: docker://dc0a29d673c17ed5d728d1e86a7b8a0b684f4e3d047c8860b0e7ebef4b21b6ed
    image: nginx:alpine
    imageID: docker-pullable://nginx@sha256:da9c94bec1da829ebd52431a84502ec471c8e548ffb2cedbf36260fd9bd1d4d3
    lastState: {}
    name: my-app
    ready: true
    restartCount: 0
    started: true
    state:
      running:
        startedAt: "2022-02-27T21:57:38Z"
  hostIP: 192.168.49.2
  phase: Running
  podIP: 172.17.0.5
  podIPs:
  - ip: 172.17.0.5
  qosClass: BestEffort
  startTime: "2022-02-27T21:57:37Z"
```

### Service

Los servicios son una abstracción _nombrada_ de un servicio de software (por ejemplo mysql) consisten de un puerto local (v.g. 3306) que el proxy escucha y el selector que determina que pods responderán la solicitud enviada a traves del proxy.

#### explain

```shell
kubectl explain service
kubectl explain service.status
kubectl explain service --recursive
```

Salida: kubectl explain service

```yaml
KIND:     Service
VERSION:  v1

DESCRIPTION:
     Service is a named abstraction of software service (for example, mysql)
     consisting of local port (for example 3306) that the proxy listens on, and
     the selector that determines which pods will answer requests sent through
     the proxy.

FIELDS:
   apiVersion   <string>
     APIVersion defines the versioned schema of this representation of an
     object. Servers should convert recognized schemas to the latest internal
     value, and may reject unrecognized values. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources

   kind <string>
     Kind is a string value representing the REST resource this object
     represents. Servers may infer this from the endpoint the client submits
     requests to. Cannot be updated. In CamelCase. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds

   metadata     <Object>
     Standard object's metadata. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata

   spec <Object>
     Spec defines the behavior of a service.
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#spec-and-status

   status       <Object>
     Most recently observed status of the service. Populated by the system.
     Read-only. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#spec-and-status
```

#### get

```shell
kubectl get svc
kubectl get service
kubectl get services
```

Salida: kubectl get services

```shell
NAME         TYPE           CLUSTER-IP     EXTERNAL-IP    PORT(S)          AGE
kubernetes   ClusterIP      10.96.0.1      <none>         443/TCP          3h29m
my-app       LoadBalancer   10.100.65.38   10.100.65.38   8000:30126/TCP   82m
```

##### get (resource) (name)

```shell
kubectl get service my-app
kubectl get services my-app
kubectl get svc my-app
kubectl get svc/my-app
kubectl get service/my-app
```

Salida: kubectl get service my-app

```shell
NAME     TYPE           CLUSTER-IP     EXTERNAL-IP    PORT(S)          AGE
my-app   LoadBalancer   10.100.65.38   10.100.65.38   8000:30126/TCP   83m
```

##### get (resource) (name) (options)

```shell
kubectl get service my-app --output=yaml
```

Salida: kubectl get service my-app --output=yaml

```yaml
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: "2022-02-27T21:57:37Z"
  labels:
    k8s-app: my-app
  name: my-app
  namespace: default
  resourceVersion: "4491"
  uid: 8cbac714-9a9e-4419-ad68-26a320889e33
spec:
  allocateLoadBalancerNodePorts: true
  clusterIP: 10.100.65.38
  clusterIPs:
  - 10.100.65.38
  externalTrafficPolicy: Cluster
  internalTrafficPolicy: Cluster
  ipFamilies:
  - IPv4
  ipFamilyPolicy: SingleStack
  ports:
  - name: tcp-8000-80-pttn6
    nodePort: 30126
    port: 8000
    protocol: TCP
    targetPort: 80
  selector:
    k8s-app: my-app
  sessionAffinity: None
  type: LoadBalancer
status:
  loadBalancer:
    ingress:
    - ip: 10.100.65.38
```

### Deployment

Los deployments permiten actualizaciones declarativas para Pods y ReplicaSets.

#### explain

```shell
kubectl explain deploy
kubectl explain deployment
kubectl explain service.status
kubectl explain service --recursive
```

Salida: kubectl explain deployment

```yaml
KIND:     Deployment
VERSION:  apps/v1

DESCRIPTION:
     Deployment enables declarative updates for Pods and ReplicaSets.

FIELDS:
   apiVersion   <string>
     APIVersion defines the versioned schema of this representation of an
     object. Servers should convert recognized schemas to the latest internal
     value, and may reject unrecognized values. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources

   kind <string>
     Kind is a string value representing the REST resource this object
     represents. Servers may infer this from the endpoint the client submits
     requests to. Cannot be updated. In CamelCase. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds

   metadata     <Object>
     Standard object's metadata. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata

   spec <Object>
     Specification of the desired behavior of the Deployment.

   status       <Object>
     Most recently observed status of the Deployment.
```

#### get

```shell
kubectl get deploy
kubectl get deployment
kubectl get deployments
```

Salida: kubectl get deployments

```shell
NAME     READY   UP-TO-DATE   AVAILABLE   AGE
my-app   1/1     1            1           126m
```

##### get (resource) (name)

```shell
kubectl get deploy my-app
kubectl get deployment my-app
kubectl get deploy/my-app
kubectl get deployment/my-app
kubectl get deployment.apps/my-app
```

Salida: kubectl get deployment my-app

```shell
NAME     READY   UP-TO-DATE   AVAILABLE   AGE
my-app   1/1     1            1           127m
```

##### get (resource) (name) (options)

```shell
kubectl get deployment my-app --output=yaml
```

Salida: kubectl get deployment my-app --output=yaml

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    deployment.kubernetes.io/revision: "1"
  creationTimestamp: "2022-02-27T21:57:37Z"
  generation: 1
  labels:
    k8s-app: my-app
  name: my-app
  namespace: default
  resourceVersion: "4488"
  uid: 14cc8452-d4eb-4bef-adc3-ae1acc356ae7
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      k8s-app: my-app
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      creationTimestamp: null
      labels:
        k8s-app: my-app
      name: my-app
    spec:
      containers:
      - image: nginx:alpine
        imagePullPolicy: IfNotPresent
        name: my-app
        resources: {}
        securityContext:
          privileged: false
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
status:
  availableReplicas: 1
  conditions:
  - lastTransitionTime: "2022-02-27T21:57:39Z"
    lastUpdateTime: "2022-02-27T21:57:39Z"
    message: Deployment has minimum availability.
    reason: MinimumReplicasAvailable
    status: "True"
    type: Available
  - lastTransitionTime: "2022-02-27T21:57:37Z"
    lastUpdateTime: "2022-02-27T21:57:39Z"
    message: ReplicaSet "my-app-68bd7d47b" has successfully progressed.
    reason: NewReplicaSetAvailable
    status: "True"
    type: Progressing
  observedGeneration: 1
  readyReplicas: 1
  replicas: 1
  updatedReplicas: 1
```

### ReplicaSets

Los ReplicaSets aseguran que un número específico de replicas pod están corriendo en un momento dado.

#### explain

```shell
kubectl explain rs
kubectl explain replicaset
kubectl explain replicasets
kubectl explain ReplicaSets
kubectl explain replicaset.status.replicas
kubectl explain replicaset --recursive
```

Salida: kubectl explain replicaset

```yaml
KIND:     ReplicaSet
VERSION:  apps/v1

DESCRIPTION:
     ReplicaSet ensures that a specified number of pod replicas are running at
     any given time.

FIELDS:
   apiVersion   <string>
     APIVersion defines the versioned schema of this representation of an
     object. Servers should convert recognized schemas to the latest internal
     value, and may reject unrecognized values. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources

   kind <string>
     Kind is a string value representing the REST resource this object
     represents. Servers may infer this from the endpoint the client submits
     requests to. Cannot be updated. In CamelCase. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds

   metadata     <Object>
     If the Labels of a ReplicaSet are empty, they are defaulted to be the same
     as the Pod(s) that the ReplicaSet manages. Standard object's metadata. More
     info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata

   spec <Object>
     Spec defines the specification of the desired behavior of the ReplicaSet.
     More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#spec-and-status

   status       <Object>
     Status is the most recently observed status of the ReplicaSet. This data
     may be out of date by some window of time. Populated by the system.
     Read-only. More info:
     https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#spec-and-status
```

#### get

```shell
kubectl get rs
kubectl get replicaset
kubectl get replicasets
kubectl get ReplicaSets
```

Salida: kubectl get replicaset

```shell
NAME               DESIRED   CURRENT   READY   AGE
my-app-68bd7d47b   1         1         1       140m
```

##### get (resource) (name)

```shell
kubectl get replicaset my-app-68bd7d47b
kubectl get replicaset my-app
kubectl get replicaset/my-app
kubectl get replicasets/my-app
kubectl get replicaset.apps/my-app-68bd7d47b
```

Salida: kubectl get replicaset my-app-68bd7d47b

```shell
NAME               DESIRED   CURRENT   READY   AGE
my-app-68bd7d47b   1         1         1       141m
```

##### get (resource) (name) (options)

```shell
kubectl get replicaset my-app-68bd7d47b --output=yaml
```

Salida: kubectl get deployment my-app --output=yaml

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  annotations:
    deployment.kubernetes.io/desired-replicas: "1"
    deployment.kubernetes.io/max-replicas: "2"
    deployment.kubernetes.io/revision: "1"
  creationTimestamp: "2022-02-27T21:57:37Z"
  generation: 1
  labels:
    k8s-app: my-app
    pod-template-hash: 68bd7d47b
  name: my-app-68bd7d47b
  namespace: default
  ownerReferences:
  - apiVersion: apps/v1
    blockOwnerDeletion: true
    controller: true
    kind: Deployment
    name: my-app
    uid: 14cc8452-d4eb-4bef-adc3-ae1acc356ae7
  resourceVersion: "4487"
  uid: 884ef8dc-aad4-41da-bb7c-77fb806a5cbb
spec:
  replicas: 1
  selector:
    matchLabels:
      k8s-app: my-app
      pod-template-hash: 68bd7d47b
  template:
    metadata:
      creationTimestamp: null
      labels:
        k8s-app: my-app
        pod-template-hash: 68bd7d47b
      name: my-app
    spec:
      containers:
      - image: nginx:alpine
        imagePullPolicy: IfNotPresent
        name: my-app
        resources: {}
        securityContext:
          privileged: false
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
status:
  availableReplicas: 1
  fullyLabeledReplicas: 1
  observedGeneration: 1
  readyReplicas: 1
  replicas: 1
```

## Cerrar Ambiente

- Detener el dashboard

```shell
Ctrl + C
```

- Detener el tunnel

```shell
Ctrl + C
```

- Detener minikube

```shell
minikube stop
```
