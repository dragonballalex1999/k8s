# LABELS AND SELECTORS

## REQUISITOS

### Clúster

Valide que el clúster se encuentra corriendo:

```Shell
minikube status
```

Salida:

```Shell
minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```

Si la salida es distinta a la anterior, inicie el clúster:

```Shell
minikube start
```

Salida:

```Shell
😄  minikube v1.25.2 on Ubuntu 20.04 (vbox/amd64)
✨  Automatically selected the docker driver
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ kubelet.housekeeping-interval=5m
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: default-storageclass, storage-provisioner
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

## DESARROLLO

### Creación de un Pod

Ejecute el siguiente comando para crear un pod sin etiquetas particulares

```SHELL
kubectl run labeled-pod-poc-1 --image=nginx:alpine
# Salida:
# pod/labeled-pod-poc-1 created
```

Consulta de los detalles del pod recién creado:

```SHELL
kubectl get pod labeled-pod-poc-1 --output=yaml
```

Salida

```SHELL
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: "2022-03-12T22:00:34Z"
  labels:
    run: labeled-pod-poc-1
  name: labeled-pod-poc-1
  namespace: default
  resourceVersion: "13908"
  uid: 5797c517-ab68-4efc-b938-cf792afe7a8b
spec:
  containers:
  - image: nginx:alpine
    imagePullPolicy: IfNotPresent
    name: labeled-pod-poc-1
    resources: {}
    terminationMessagePath: /dev/termination-log
    terminationMessagePolicy: File
    volumeMounts:
    - mountPath: /var/run/secrets/kubernetes.io/serviceaccount
      name: kube-api-access-6hpv6
      readOnly: true
  dnsPolicy: ClusterFirst
  enableServiceLinks: true
  nodeName: minikube
  preemptionPolicy: PreemptLowerPriority
  priority: 0
  restartPolicy: Always
  schedulerName: default-scheduler
  securityContext: {}
  serviceAccount: default
  serviceAccountName: default
  terminationGracePeriodSeconds: 30
  tolerations:
  - effect: NoExecute
    key: node.kubernetes.io/not-ready
    operator: Exists
    tolerationSeconds: 300
  - effect: NoExecute
    key: node.kubernetes.io/unreachable
    operator: Exists
    tolerationSeconds: 300
  volumes:
  - name: kube-api-access-6hpv6
    projected:
      defaultMode: 420
      sources:
      - serviceAccountToken:
          expirationSeconds: 3607
          path: token
      - configMap:
          items:
          - key: ca.crt
            path: ca.crt
          name: kube-root-ca.crt
      - downwardAPI:
          items:
          - fieldRef:
              apiVersion: v1
              fieldPath: metadata.namespace
            path: namespace
status:
  conditions:
  - lastProbeTime: null
    lastTransitionTime: "2022-03-12T22:00:34Z"
    status: "True"
    type: Initialized
  - lastProbeTime: null
    lastTransitionTime: "2022-03-12T22:00:35Z"
    status: "True"
    type: Ready
  - lastProbeTime: null
    lastTransitionTime: "2022-03-12T22:00:35Z"
    status: "True"
    type: ContainersReady
  - lastProbeTime: null
    lastTransitionTime: "2022-03-12T22:00:34Z"
    status: "True"
    type: PodScheduled
  containerStatuses:
  - containerID: docker://5d2cf4891fdd355d23390facfc51d34588f302e169309f24197ea834ec063d29
    image: nginx:alpine
    imageID: docker-pullable://nginx@sha256:da9c94bec1da829ebd52431a84502ec471c8e548ffb2cedbf36260fd9bd1d4d3
    lastState: {}
    name: labeled-pod-poc-1
    ready: true
    restartCount: 0
    started: true
    state:
      running:
        startedAt: "2022-03-12T22:00:35Z"
  hostIP: 192.168.49.2
  phase: Running
  podIP: 172.17.0.3
  podIPs:
  - ip: 172.17.0.3
  qosClass: BestEffort
  startTime: "2022-03-12T22:00:34Z"
```

Consulta con las etiquetas del recurso:

```SHELL
kubectl get pod labeled-pod-poc-1 --show-labels
# Salida:
# NAME                READY   STATUS    RESTARTS   AGE     LABELS
# labeled-pod-poc-1   1/1     Running   0          3m55s   run=labeled-pod-poc-1
```

### Adición de Etiquetas

El comando `label` permite adicionar etiquetas a un objeto.

Adición de dos etiquetas:

```SHELL
kubectl label pod labeled-pod-poc-1 {env=poc,app=nginxy} --dry-run=client
# Salida:
# pod/labeled-pod-poc-1 labeled
```

Recuerde que la bandera `--dry-run=client` permite _probar_ la instrucción, y no realiza cambios.

Adición de una etiqueta:

```SHELL
kubectl label pod labeled-pod-poc-1 wrongLabel=invalid
```

Consulte con las etiquetas del recurso:

```SHELL
kubectl get pod labeled-pod-poc-1 --show-labels
# Salida:
# NAME                READY   STATUS    RESTARTS   AGE   LABELS
# labeled-pod-poc-1   1/1     Running   0          11m   app=nginxy,env=poc,run=labeled-pod-poc-1,wrongLabel=invalid
```

### Eliminación de Etiquetas

El comando `label` provee la siguiente sintaxis para la eliminación de una etiqueta:

```SHELL
kubectl label pod labeled-pod-poc-1 wrongLabel-
# Salida:
# pod/labeled-pod-poc-1 unlabeled
```

### Selectores

Analice el siguiente bloque de código:

```YAML
apiVersion: v1
kind: Pod
metadata:
  name: labeled-pod-poc-2
  labels:
    env: poc
    app: nginx
spec:
  containers:
  - name: nginx
    image: nginx:alpine
    ports:
    - containerPort: 80
---
apiVersion: v1
kind: Pod
metadata:
  name: labeled-pod-test-3
  labels:
    env: test
    app: nginx
spec:
  containers:
  - name: nginx
    image: nginx:alpine
    ports:
    - containerPort: 80
```

Ejecute el comando para la _aplicación_ de los pods definidos en el archivo mostrado:

```
kubectl apply --filename=02_label_pods-2.yaml --dry-run=client
# Salida
# pod/labeled-pod-poc-2 created
# pod/labeled-pod-test-3 created
```

Realize la ejecución de los siguientes comandos:

```Shell
kubectl get pods -l app=nginx
kubectl get pods --selector app=nginx
kubectl get pods --selector=app=nginx
kubectl get pods --selector env=poc
kubectl get pods --selector env=test
kubectl get pods -l 'env in (poc,test)'
kubectl get pods --selector='env in (poc,test)'
```

#### Discusión (5min)

Comente los resultados de las ejecuciones anteriores.

### Modificación de una Etiqueta

Además de eliminar y crear nuevamente la etiqueta existe la posibilidad de actualizar su valor con el comando `label` y la bandera `--overwrite=true`:

```Shell
kubectl label pod labeled-pod-poc-1 app=nginx --overwrite=true
# Salida
# pod/labeled-pod-poc-1 unlabeled
```

Valide que ahora se obtiene los tres pods con la etiqueta `app=nginx`:

```Shell
kubectl get pods -l app=nginx
kubectl get pods --selector=app=nginx
kubectl get pods --selector='app in (nginx)'
```

Salida

```Shell
NAME                 READY   STATUS    RESTARTS   AGE   LABELS
labeled-pod-poc-1    1/1     Running   0          45m   app=nginx,env=poc,run=labeled-pod-poc-1
labeled-pod-poc-2    1/1     Running   0          15m   app=nginx,env=poc
labeled-pod-test-3   1/1     Running   0          15m   app=nginx,env=test
```

### Discusión (3min)

Realize la ejecución del siguiente comando:

```Shell
kubectl get pods --selector='env notin (poc),app=nginx'
```

Comente los resultados.

```Shell
NAME                 READY   STATUS    RESTARTS   AGE
labeled-pod-test-3   1/1     Running   0          18m
```

### Servicio

Analice la siguiente definición:

```YAML
apiVersion: v1
kind: Service
metadata:
  name: selector-poc-service
  namespace: default
  labels:
    env: poc
    app: nginx
spec:
  externalTrafficPolicy: Local
  ports:
  - name: http
    port: 80
    protocol: TCP
    targetPort: 80
  selector:
    env: poc
  type: NodePort
```

El bloque anterior crea un servicio que expone los pods con la etiqueta `env: poc`.

Ejecute el siguiente comando con la especificación previamente mostrada:

```Shell
kubectl apply --filename=02_label_02_service.yaml
```

Consulte  la información del servicio recién creado:

```Shell
kubectl get service selector-poc-service --output=wide
```

Salida

```Shell
NAME                   TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE     SELECTOR
selector-poc-service   NodePort   10.109.108.87   <none>        80:32154/TCP   7m17s   env=poc
```

Con la _IP_ indicada en la columna _CLUSTER-IP_ puede ir a un navegador web y el servicio obtendrá la respuesta del pod que corresponda.

![Navegador con nginx](../media/02_02_LAB_01_WB-con-nginx.png "Navegador con App Nginx")

Para obtener la descripción detallada de la configuración:

```SHELL
kubectl get service selector-poc-service --output=YAML
```

Salida

```YAML
apiVersion: v1
kind: Service
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"v1","kind":"Service","metadata":{"annotations":{},"labels":{"app":"nginx","env":"poc"},"name":"selector-poc-service","namespace":"default"},"spec":{"externalTrafficPolicy":"Local","ports":[{"name":"http","port":80,"protocol":"TCP","targetPort":80}],"selector":{"env":"poc"},"type":"NodePort"}}
  creationTimestamp: "2022-03-12T22:59:09Z"
  labels:
    app: nginx
    env: poc
  name: selector-poc-service
  namespace: default
  resourceVersion: "16392"
  uid: 41ffc165-4a85-44dd-bfe2-ac54ca92a239
spec:
  clusterIP: 10.109.108.87
  clusterIPs:
  - 10.109.108.87
  externalTrafficPolicy: Local
  internalTrafficPolicy: Cluster
  ipFamilies:
  - IPv4
  ipFamilyPolicy: SingleStack
  ports:
  - name: http
    nodePort: 32154
    port: 80
    protocol: TCP
    targetPort: 80
  selector:
    env: poc
  sessionAffinity: None
  type: NodePort
status:
  loadBalancer: {}
```

### Endpoints

Para los servicios con selectores se crean automáticamente los objetos `Endpoints` que se encargan de ser el punto de entrada/salida de las peticiones hacia los pods.

Para conocer los endpoints existentes pude ejecutar:

```Shell
kubectl get ep
kubectl get endpoints
kubectl get endpoints selector-poc-service
```

Salida:  `kubectl get endpoints selector-poc-service`

```Shell
NAME                   ENDPOINTS                     AGE
selector-poc-service   172.17.0.3:80,172.17.0.5:80   19m
```

Para conocer el detalle del endpoint:

```SHELL
kubectl get endpoints selector-poc-service --output=YAML
```

Salida

```YAML
apiVersion: v1
kind: Endpoints
metadata:
  annotations:
    endpoints.kubernetes.io/last-change-trigger-time: "2022-03-12T22:59:09Z"
  creationTimestamp: "2022-03-12T22:59:09Z"
  labels:
    app: nginx
    env: poc
  name: selector-poc-service
  namespace: default
  resourceVersion: "16394"
  uid: 693d96cb-7ed0-4e9f-aed9-06acd2edec31
subsets:
- addresses:
  - ip: 172.17.0.3
    nodeName: minikube
    targetRef:
      kind: Pod
      name: labeled-pod-poc-1
      namespace: default
      resourceVersion: "15694"
      uid: 5797c517-ab68-4efc-b938-cf792afe7a8b
  - ip: 172.17.0.5
    nodeName: minikube
    targetRef:
      kind: Pod
      name: labeled-pod-poc-2
      namespace: default
      resourceVersion: "15194"
      uid: df21cc50-62f7-4e6f-9280-31a42a66efb0
  ports:
  - name: http
    port: 80
    protocol: TCP
```

Al analizar el nodo `.subsets.addresses` se puede identificar las referencias a los pods que cumplen con las etiquetas indicadas en el selector.

## RESTABLECIMIENTO

Para eliminar los elementos creados ejecute los siguientes comandos:

```Shell
kubectl delete service selector-poc-service
# Salida
# service "selector-poc-service" deleted
```

La ejecución anterior elimina automáticamente el endpoint asociado.

También puede realizar acciones para bloques de elementos como se muestra a continuación:

```Shell
kubectl delete pods --selector='app in (nginx)'
# Salida
# pod "labeled-pod-poc-1" deleted
# pod "labeled-pod-poc-2" deleted
# pod "labeled-pod-test-3" deleted
```

## REFERENCIAS

- [Labels and Selectors](https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/)
