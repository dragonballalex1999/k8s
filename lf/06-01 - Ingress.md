# INGRESS

Un Ingress es un objeto API que define reglas que permiten el acceso externo a los servicios en un clúster. Un controlador Ingress cumple con las reglas establecidas en Ingress.

Esta actividad muestra como configurar un Ingress que rutea las solicitudes al servicio web o web2 según el URI de HTTP

## REQUISITOS

### Clúster

Valide que el clúster se encuentra corriendo:

```Shell
minikube status
```

Salida:

```Shell
minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
```

Si la salida es distinta a la anterior, inicie el clúster:

```Shell
minikube start
```

Salida:

```Shell
😄  minikube v1.25.2 on Ubuntu 20.04 (vbox/amd64)
✨  Automatically selected the docker driver
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🐳  Preparing Kubernetes v1.23.3 on Docker 20.10.12 ...
    ▪ kubelet.housekeeping-interval=5m
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: default-storageclass, storage-provisioner
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

#### Addon Ingress

Para habilitar el controlador Ingress para NGINX en minikube ejecute:

```Shell
minikube addons enable ingress
```

Salida

```Shell
    ▪ Using image k8s.gcr.io/ingress-nginx/kube-webhook-certgen:v1.1.1
    ▪ Using image k8s.gcr.io/ingress-nginx/controller:v1.1.1
    ▪ Using image k8s.gcr.io/ingress-nginx/kube-webhook-certgen:v1.1.1
🔎  Verifying ingress addon...
🌟  The 'ingress' addon is enabled
```

Para verificar que el controlador se está ejecutando ejecute:

```Shell
kubectl get pods -n ingress-nginx
```

Salida:

```Shell
ingress-nginx-admission-create-79n67       0/1     Completed   0          51s
ingress-nginx-admission-patch-h6w2n        0/1     Completed   1          51s
ingress-nginx-controller-cc8496874-fwwtw   1/1     Running     0          51s
```

## DESARROLLO

### Despliegue de la v1

Cree un `deployment` con el comando:

```shell
kubectl create deployment web --image=gcr.io/google-samples/hello-app:1.0
```

Salida

```Shell
deployment.apps/web created
```

Se expone el `deployment` con el comando:

```Shell
kubectl expose deployment web --type=NodePort --port=8080
```

Salida:

```Shell
service/web exposed
```

Verifique que el servicio se haya creado y esté disponible en un NodePort:

```shell
kubectl get service web
```

La salida es similar a:

```Shell
NAME   TYPE       CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
web    NodePort   10.101.33.40   <none>        8080:30989/TCP   3m7s
```

Visite el Servicio a través de NodePort:

```shell
minikube service web --url
```

La salida es similar a:

```Shell
http://192.168.49.2:30989
```

Ahora realice una solicitud web para validar el acceso:

```Shell
curl http://192.168.49.2:30989
```

La salida es similar a:

```Shell
Hello, world!
Version: 1.0.0
Hostname: web-746c8679d4-9lpgf
```

Ahora que tenemos acceso a la aplicación a través de la dirección IP de Minikube y NodePort. El siguiente paso permite acceder a la aplicación mediante el recurso `Ingress`.

### Creación de un Ingress

El siguiente manifiesto define un Ingress que envía tráfico al servicio a través de `hello-world.info`.

Cree un archivo `06_01_INGRESS_1.yaml`a partir del siguiente archivo:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: example-ingress
  labels:
    env: poc
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /$1
spec:
  rules:
    - host: hello-world.info
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: web
                port:
                  number: 8080
```

Cree el objeto Ingress ejecutando el siguiente comando:

```shell
kubectl apply --filename=06_01_INGRESS_1.yaml
```

La salida debe ser:

```Shell
ingress.networking.k8s.io/example-ingress created
```

Verifique que la dirección IP esté configurada:

```shell
kubectl get ingress
```

**Nota:** Esto puede tardar un par de minutos.

Debería ver una dirección IPv4 en la columna `ADDRESS`, por ejemplo:

```Shell
NAME              CLASS   HOSTS              ADDRESS        PORTS   AGE
example-ingress   nginx   hello-world.info   192.168.49.2   80      85s
```

**Nota:** En minikube existe `minikube ip` para obtener la IP.

Agregue la siguiente línea al final del archivo `/etc/hosts` del equipo de trabajo (necesitará acceso de administrador):

```Shell
192.168.49.2 hello-world.info
```

El archivo debe quedar semejante a:

```Shell
127.0.0.1       localhost
127.0.1.1       n-introk8s
192.168.49.2    hello-world.info

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
```

Después de realizar este cambio, su navegador web envía solicitudes de URL de hello-world.info a Minikube.

Verifique que el controlador de Ingress esté dirigiendo el tráfico:

```shell
curl hello-world.info
```

La salida es semejante a:

```Shell
Hello, world!
Version: 1.0.0
Hostname: web-746c8679d4-9lpgf
```

Al visitar `hello-world.info` desde su navegador:

![Webapp e Ingress](../media/06_01_LAB_Ingress-webapp.png "Webapp e Ingress")

### Despliegue de la v2

Cree otra implementación con el siguiente comando:

```shell
kubectl create deployment web2 --image=gcr.io/google-samples/hello-app:2.0
```

La salida debe ser:

```Shell
deployment.apps/web2 created
```

Exponga el segundo `deployment`:

```shell
kubectl expose deployment web2 --port=8080 --type=NodePort
```

La salida debe ser:

```Shell
service/web2 exposed
```

### Actualización del Ingress

Compare el contenido del bloque siguiente (06_02_INGRESS_2.yaml) con el del Ingress actualmente desplegado.

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: example-ingress
  labels:
    env: poc
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /$1
spec:
  rules:
    - host: hello-world.info
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: web
                port:
                  number: 8080
          - path: /v2
            pathType: Prefix
            backend:
              service:
                name: web2
                port:
                  number: 8080
```

Aplicar los cambios:

```shell
kubectl apply --filename=06_01_INGRESS_2.yaml --dry-run=server
```

Deberías ver:

```Shell
ingress.networking.k8s.io/example-ingress configured
```

Observe el detalle del Ingress:

```Shell
kubectl describe ingress example-ingress
```

La salida debe ser semejante a:

```Shell
Name:             example-ingress
Labels:           env=poc
Namespace:        default
Address:          192.168.49.2
Default backend:  default-http-backend:80 (<error: endpoints "default-http-backend" not found>)
Rules:
  Host              Path  Backends
  ----              ----  --------
  hello-world.info  
                    /     web:8080 (172.17.0.7:8080)
                    /v2   web2:8080 (172.17.0.8:8080)
Annotations:        nginx.ingress.kubernetes.io/rewrite-target: /$1
Events:
  Type    Reason  Age                From                      Message
  ----    ------  ----               ----                      -------
  Normal  Sync    19m (x3 over 45m)  nginx-ingress-controller  Scheduled for sync
```

## Prueba

Acceda a la primera versión de la aplicación Hello World.

```shell
curl hello-world.info
```

La salida es semejante a:

```Shell
Hello, world!
Version: 1.0.0
Hostname: web-746c8679d4-9lpgf
```

Accede a la segunda versión de la aplicación web.

```shell
curl hello-world.info/v2
```

La salida es semejante a:

```Shell
Hello, world!
Version: 2.0.0
Hostname: web2-5858b4c7c5-vzf25
```

## RESTABLECIMIENTO

### Elementos Creados

Ninguna acción ya que se restablecerá el clúster completamente.

### Clúster

Para restablecer el clúster realice los siguientes pasos:

i. Se detiene `minikube`:

```Shell
 minikube stop
```

Salida:

```Shell
✋  Stopping node "minikube"  ...
🛑  Powering off "minikube" via SSH ...
✋  Stopping node "minikube-m02"  ...
🛑  Powering off "minikube-m02" via SSH ...
✋  Stopping node "minikube-m03"  ...
🛑  Powering off "minikube-m03" via SSH ...
🛑  3 nodes stopped.
```

ii. Se elimina el clúster:

```Shell
minikube delete --all
```

Salida:

```Shell
🔥  Deleting "minikube" in docker ...
🔥  Removing /home/netec/.minikube/machines/minikube ...
💀  Removed all traces of the "minikube" cluster.
🔥  Successfully deleted all profiles
```

## REFERENCIAS

- [Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/)
- [Set up Ingress on Minikube with the NGINX Ingress Controller](https://kubernetes.io/docs/tasks/access-application-cluster/ingress-minikube/)
- [NGINX Ingress Controller](https://kubernetes.github.io/ingress-nginx/)